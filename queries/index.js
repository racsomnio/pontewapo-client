import gql from 'graphql-tag';

export const UPLOAD_IMAGE = gql`
    mutation($img64base: String){
        uploadImage(img64base: $img64base){
            secure_url
            public_id
            format
        }
    }
`;

export const STORE_PIC = gql`
    mutation($img64base: String, $_id: ID){
        storePic(img64base: $img64base, _id: $_id){
            message
        }
    }
`;

/* User Queries */
export const GET_CURRENT_USER = gql`
    query{
        getCurrentUser{
            __typename
            ... on User {
                _id
                username
                email
                createdAt
                isGood{
                    _id
                    name
                }
            }
            ... on Store {
                _id
                name
                email
                slug
                rights
                stripeCustomerId
                location {
                    address
                    coordinates
                }
                full_menu {
                    _id
                }
            }
        }
    }
`;

/* User Mutations */
export const SINGNUP_USER = gql`
    mutation($username: String!, $email: String!, $password: String!){
        signupUser(username: $username, email: $email, password: $password){
            token
        }
    }
`;

export const SINGNIN_USER = gql`
    mutation($email: String!, $password: String!){
        signinUser(email: $email, password: $password){
            __typename
        }
    }
`;

export const REQUEST_RESET = gql`
    mutation($email: String!){
        requestReset(email: $email){
            message
        }
    }
`;

export const RESET_PASSWORD = gql`
    mutation($resetToken: String!, $password: String!){
        resetPassword(resetToken: $resetToken, password: $password,){
            __typename
        }
    }
`;

export const SIGNOUT = gql`
    mutation{
        signout{
            message
        }
    }
`;

/* Store Queries */

export const GET_MY_STORE = gql`
    query($_id: ID){
        getMyStore(_id: $_id){
            _id
            name
            location{
                address
                coordinates
            }
            phone{
                number
                whatsapp
            }
            typeOfBusiness
            cover_img{
                secure_url
            }
            profile_img{
                secure_url
            }
            food_type {
                code
                label
                type
            }
            hours {
                from
                to
                open_hr
                open_min
                open_ampm
                close_hr
                close_min
                close_ampm
            }
            speciality
            email
            wa_orders
            delivery
            takeaway
            only_cash
            card
            open_tv
            cable
            ppv
            speak_english
            english_menu
            vegan_option
            vegetarian_option
            no_gluten_option
            no_shellfish_option
            reservations
            live_music
            dancing
            rooftop
            terrace
            garden
            smoking_area
            kids_menu
            kids_area
            wifi
            private_room
            parking
            valet_parking
            pet_friendly
            catering
            gay
            has_breakfast
            has_lunch_special
            has_healthy_food
            has_alcohol
            has_billiard
            has_pool
            has_foosball
            has_table_games
            has_karaoke
            big_groups
            etiquette
        }
    }
`;

export const GET_MY_PICS = gql`
    query($_id: ID){
        getMyStore(_id: $_id){
            stored_pics
        }
    }
`;

export const GET_ALL_STORES = gql`
    query{
        getAllStores {
            _id
            name
            email
            phone{
                number
            }
            typeOfBusiness
            location{
                address
                coordinates
            }
            createdDate
            menus {
                _id
                meal
                repeat
            }
            profile_img {
                secure_url
            }
        }
    }
`;

export const GET_STORE = gql`
    query($_id: ID!){
        getStore(_id: $_id){
            _id
            name
            location{
                address
                coordinates
            }
            phone{
                number
                whatsapp
            }
            typeOfBusiness
            cover_img{
                secure_url
            }
            profile_img{
                secure_url
            }
            speciality
            food_type {
                code
                type
            }
            email
            wa_orders
            delivery
            takeaway
            only_cash
            card
            open_tv
            cable
            ppv
            speak_english
            english_menu
            vegan_option
            vegetarian_option
            no_gluten_option
            no_shellfish_option
        }
    }
`;

export const GET_STORE_BY_SLUG = gql`
    query($slug: String!){
        getStoreBySlug(slug: $slug){
            _id
            rights
            name
            location{
                address
                coordinates
            }
            phone{
                number
                whatsapp
            }
            typeOfBusiness
            cover_img{
                secure_url
            }
            profile_img{
                secure_url
            }
            food_type {
                code
                type
            }
            hours {
                    _id
                    from
                    to
                    open_hr
                    open_min
                    open_ampm
                    close_hr
                    close_min
                    close_ampm
                }
            speciality
            wa_orders
            delivery
            takeaway
            only_cash
            card
            open_tv
            cable
            ppv
            speak_english
            english_menu
            vegan_option
            vegetarian_option
            no_gluten_option
            no_shellfish_option
            reservations
            live_music
            dancing
            rooftop
            terrace
            garden
            smoking_area
            kids_menu
            kids_area
            wifi
            private_room
            parking
            valet_parking
            pet_friendly
            catering
            gay
            has_breakfast
            has_lunch_special
            has_healthy_food
            has_alcohol
            has_billiard
            has_pool
            has_foosball
            has_karaoke
            big_groups
            etiquette
            
            full_menu {
                _id
                menu {
                    section_title
                    section_description
                    section_footer
                    section_image
                    food_items {
                        food_name
                        food_image
                        food_price {
                            price_value
                            price_description
                        }
                        food_description
                        food_tags
                        active
                    }
                    active
                }
                customized {
                    page_background_color
                    title_background_color
                    title_text_color
                    items_background_color
                    items_text_color
                }
            }
        }
    }
`;

/* Store Mutations */
export const CREATE_STORE = gql`
    mutation(
        $name: String!, 
        $email: String!, 
        $password: String!, 
        $location: LocationInput, 
        $typeOfBusiness: [String]!, 
        $phone: [PhoneInput])
    {
        createStore(
            name: $name, 
            email: $email, 
            password: $password, 
            location: $location, 
            typeOfBusiness: $typeOfBusiness, 
            phone: $phone){
                __typename
            }
    }
`;

export const UPDATE_STORE_IMGS = gql`
    mutation(
        $_id: ID,
        $flag: String,
        $img64base: String
    ){
        updateStoreImgs(_id: $_id, flag: $flag, img64base: $img64base){
            _id
            name
            cover_img {
                secure_url
            }
            profile_img {
                secure_url
            }
        }
    }
`;

export const UPDATE_STORE_BOOLEANS = gql`
    mutation(
        $_id: ID,
        $flag: String,
        $res: Boolean
    ){
        updateStoreBooleans(_id: $_id, flag: $flag, res: $res){
            delivery
        }
    }
`;

export const UPDATE_STORE_PHONES = gql`
    mutation(
        $_id: ID,
        $phones: [PhoneInput]
    ){
        updateStorePhones(_id: $_id, phones: $phones){
            phone{
                number
                whatsapp
            }
        }
    }
`;

export const UPDATE_STORE_FOOD_TYPE = gql`
    mutation($food_type: CountryInput ){
        updateStoreFoodType(food_type: $food_type){
            food_type {
                code
                label
                type
            }
        }
    }
`;

export const UPDATE_STORE_SPECIALITY = gql`
    mutation($speciality: [String] ){
        updateStoreSpeciality(speciality: $speciality){
            speciality
        }
    }
`;

export const UPDATE_STORE_HOURS = gql`
    mutation($hours: [HoursInput] ){
        updateStoreHours(hours: $hours){
            hours {
                from
                to
            }
        }
    }
`;

export const CREATE_STRIPE_SESSION = gql`
    mutation($plan: String! ){
        createStripeSession(plan: $plan){
            message
        }
    }
`;

export const CREATE_STRIPE_SUBSCRIPTION = gql`
    mutation($stripeSessionId: String! ){
        createStripeSubscription(stripeSessionId: $stripeSessionId){
            email
        }
    }
`;

/* Menu Queries */
export const GET_MENUS = gql`
    query($coordinates: String, $meal: AllowedMeals){
        getMenus(coordinates:$coordinates, meal: $meal){
            _id
            meal
            repeat
            menu_image {
                secure_url
            }
            location{
                coordinates
            }
            owner{
                _id
                name
                takeaway
                delivery
                only_cash
                card
                typeOfBusiness
                speciality
                food_type {
                    code
                    type
                }
                location {
                    address
                    coordinates
                }
                profile_img{
                    secure_url
                }
            }
            distance
        }
    }
`;

export const GET_MENU = gql`
    query($_id: ID!){
        getMenu(_id: $_id){
            _id
            meal
            expires_at
            repeat
            html_menu
            price
            hours
            promotion
            menu_image{
                secure_url
            }
            location{
                address
                coordinates
            }
            owner{
                _id
                rights
                name
                email
                typeOfBusiness
                phone{
                    number
                    whatsapp
                }
                profile_img{
                    secure_url
                }
                food_type {
                    code
                    type
                }
                hours {
                    from
                    to
                    open_hr
                    open_min
                    open_ampm
                    close_hr
                    close_min
                    close_ampm
                }
                speciality
                delivery
                takeaway
                only_cash
                card
                open_tv
                cable
                ppv
                speak_english
                english_menu
                vegan_option
                vegetarian_option
                no_gluten_option
                no_shellfish_option
                reservations
                live_music
                dancing
                rooftop
                terrace
                garden
                smoking_area
                kids_menu
                kids_area
                wifi
                private_room
                parking
                valet_parking
                pet_friendly
                catering
                gay
                has_breakfast
                has_lunch_special
                has_healthy_food
                has_alcohol
                has_billiard
                has_pool
                has_foosball
                has_karaoke
                big_groups
                etiquette
            }
            distance
        }
    }
`;

export const GET_MY_MENUS = gql`
    query($_id: ID){
        getMyMenus(_id:$_id){
            _id
            meal
            repeat
            menu_image {
                secure_url
            }
        } 
    }
`;

/* Menu Mutations */
export const CREATE_MENU = gql`
    mutation(
        $meal: String, 
        $repeat: [Int], 
        $html_menu: String, 
        $price: String, 
        $hours: String, 
        $promotion: String, 
        $location: LocationInput, 
        $menu_image: [CloudinaryInput],
        $owner: String
        ){
            createMenu(
                meal: $meal, 
                repeat: $repeat, 
                html_menu: $html_menu, 
                price: $price, 
                hours: $hours, 
                promotion: $promotion, 
                location: $location, 
                menu_image: $menu_image,
                owner: $owner
            ){
                _id
                meal
                repeat
                html_menu
                promotion
                menu_image {
                    secure_url
                }
                hours
                price
                expires_at
            }
        }
`;

export const UPDATE_MENU = gql`
    mutation(
        $_id:ID!, 
        $meal: String, 
        $repeat: [Int], 
        $html_menu: String, 
        $menu_image: [CloudinaryInput],
        $price: String, 
        $hours: String, 
        $promotion: String, 
    ){
    updateMenu(
            _id:$_id, 
            meal: $meal, 
            repeat: $repeat, 
            html_menu: $html_menu, 
            menu_image: $menu_image,
            price: $price, 
            hours: $hours, 
            promotion: $promotion, 
        ){
            _id
            meal
            repeat
            html_menu
            menu_image {
                secure_url
            }
            owner{
                _id
                email
            }
        } 
    }
`;

export const DELETE_MENU = gql`
    mutation($menuId: ID!, $ownerId: ID!) {
        deleteMenu(
            menuId: $menuId, 
            ownerId: $ownerId
        ){
            meal
            repeat
        }
    }
`;


export const CREATE_FULL_MENU = gql`
    mutation(
        $menu: [MenuSectionsInput], 
        $ownerId: String,
        ){
            createFullMenu(
                menu: $menu, 
                ownerId: $ownerId
            ){
                _id
                menu {
                    section_title
                }
            }
        }
`;

export const UPDATE_FULL_MENU = gql`
    mutation(
        $fullMenuId: String,
        $menu: [MenuSectionsInput], 
        $ownerId: String,
        ){
            updateFullMenu(
                fullMenuId: $fullMenuId,
                menu: $menu, 
                ownerId: $ownerId
            ){
                _id
                menu {
                    section_title
                }
            }
        }
`;

export const UPDATE_CUSTOMIZED_FULL_MENU = gql`
    mutation(
        $fullMenuId: String,
        $customized: PickColorsInput, 
        ){
            updateCustomized(
                fullMenuId: $fullMenuId,
                customized: $customized, 
            ){
                _id
                customized {
                    page_background_color
                    title_background_color
                    title_text_color
                    items_background_color
                    items_text_color
                }
            }
        }
`;

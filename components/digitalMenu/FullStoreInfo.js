import { useState, useRef, useEffect } from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Headroom from 'react-headroom'
import smoothscroll from 'smoothscroll-polyfill';
import dynamic from 'next/dynamic'
import { useQuery } from "@apollo/react-hooks"
import styled, { keyframes, css } from 'styled-components'
import {useTransition, animated} from 'react-spring'
import { GET_STORE_BY_SLUG, GET_CURRENT_USER } from '../../queries'
import Loading from '../common/Loading'
import Error from '../common/Error'
import { Icon } from '../../styles/common';
import FullMenuCard from '../store/FullMenuCard'
import ShowLocation from '../common/ShowLocation'
import { Divider } from '../../styles/common'
import Modal from '../common/Modal'
import AskUserInfo from './AskUserInfo'
const ShareButtons = dynamic(
    () => import('../menu/ShareButtons'),
    { ssr: false }
)

const initialUser = {
    entrega: '',
    nombre: '',
    calle: '',
    num: '',
    apt: '',
    cp: '',
    pago: '',
}

const FullStoreInfo = ({ slug }) => {
    const { loading: userLoading, data: userData } = useQuery(GET_CURRENT_USER);
    const { loading, error, data } = useQuery(GET_STORE_BY_SLUG, {
        variables: { slug },
    });
    const sectionRefs= useRef({});
    const sectionsWrapperRef = useRef();
    
    const [ order, setOrder] = useState([]);
    const [ showSections, setShowSections ] = useState(false)
    const [ showOrderList, setShowOrderList ] = useState(false)
    const [user, setUser] = useState({...initialUser})

    const transitions = useTransition(!!order.length, null, {
        from: { position: 'fixed', zIndex:'1', bottom: 0, left: '50%', transform: 'translate(-50%, 100%)' },
        enter: { transform: 'translate(-50%, 1%)' },
        leave: { transform: 'translate(-50%, 100%)' },
    })

    useEffect(() => {
        smoothscroll.polyfill();
    }, [])

    useEffect(() => {
        if(sectionsWrapperRef.current) {
            const wrapperHeight = sectionsWrapperRef.current.offsetHeight;
            sectionsWrapperRef.current.style.height = `${wrapperHeight}px`;
        }
    })

    if( loading ) return <Loading />
    if( error ) return <Error error={error}/>

    const { _id, name, profile_img, cover_img, typeOfBusiness, location, food_type, phone,
        delivery, takeaway, only_cash,card, reservations, has_breakfast, has_lunch_special, parking, valet_parking,
        wifi, open_tv, cable, ppv, speak_english, english_menu, vegan_option, vegetarian_option, no_gluten_option,
        no_shellfish_option, live_music, dancing, has_karaoke, rooftop, terrace, garden, smoking_area, kids_menu,
        kids_area, private_room, pet_friendly, catering, gay, has_healthy_food, has_alcohol, has_billiard,
        has_foosball, has_pool, big_groups, hours, speciality, full_menu, rights, wa_orders } = data.getStoreBySlug;
    
    const active_full_menu = full_menu.menu.filter(section => section.active === null || section.active)
    const has_rights = !rights.includes('FREE');
    const regex = /[^"]?https.+?(?=")/g;
    const images = JSON.stringify(active_full_menu);
    const has_images = images.match(regex)
    const has_whatsapp = phone.filter( ph => ph.whatsapp);
    const use_whatsapp = has_whatsapp.length ? has_whatsapp[0].number : "";

    const scrollToRef = (ref) => {
        window.scroll({ top: sectionRefs.current[ref].offsetTop - 90, left: 0, behavior: 'smooth' })
    };

    function addItem (item) {
        const already = order.filter(co => {
            if(co.food_name === item.food_name && co.price[0].price_description !== item.price[0].price_description) return item
            if(co.food_name !== item.food_name) return item
        })
        already.push(item)
        setOrder(already);
    }

    function encodeOrder () {
        let summarize = "Hola, me gustaría hacer el siguiente pedido:\n\n";
        const total = order.reduce((a,b) => {
            return a + b.price[0].price_value * b.qty
        }, 0)
        order.forEach(o => {
            return summarize += `*${o.qty} - ${o.food_name.trim()}* ${o.price[0].price_description && `*(${o.price[0].price_description})*`} \n${o.special_request && `NOTA: ${o.special_request}\n`}\n`
        } )
        summarize += `_________________________ \n total: *$${total}* (Favor de confirmar)`

        summarize += `\n\n*Forma de entrega:*\n ${user.entrega === 'delivery' ? '🛵 A domicilio' : '🚶🏻Lo paso a recoger'}`
        summarize += `\n*A nombre de:* ${user.nombre}`
        if(user.entrega === 'delivery') {
            const address = `${user.calle} ${user.num}${user.apt && ` Apt ${user.apt}`}, C.P. ${user.cp}`
            const format_address = address.replace(/ /g, '+');
            summarize += `\n*Dirección:* ${address}\nhttps://www.google.com/maps/dir/?api=1&destination=${format_address}&travelmode=bicycling`
        }

        return encodeURIComponent(summarize)
    }

    function closeModal() {
        setShowOrderList(false)
    }

    function handleUserDetailsOnChange(e) {
        const {value, name} = e.target;
        console.log(value,'+', name)
        setUser(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    return (
        <>
            <Head>
                <title>{name}</title>
                <meta property="og:title" content={name} />
                <meta property="og:image" content={has_images && has_images[0]} />
                <meta property="og:type" content="website" />
                <meta name="description" property="og:description" content={`Te invitamos a ver nuestro menú. Esperamos verte pronto por aquí.`} />
                <meta property="og:locale" content="es_MX" />
                <meta property='twitter:title' content={name} />
                <meta property='twitter:image' content={has_images && has_images[0]}/>
                <meta name="twitter:card" content="summary_large_image"/>
            </Head>

            <Container>
                {
                    userData && (userData.getCurrentUser && (_id === userData.getCurrentUser._id)) && 
                    <>  
                        <p className="message"><small>Sólo tú puedes ver estos botones.</small></p>
                        <Link href="/negocios/mi-menu">
                            <a className="button secondary">
                                Seguir Editando
                            </a>
                        </Link>
                        {
                            has_rights &&
                            <Link href="/negocios/personalizar-menu">
                                <a className="button secondary">
                                    Personalizar
                                </a>
                            </Link>
                        }
                        <Link href="/negocios/qr-code">
                            <a className="button">
                                Mi Código QR
                            </a>
                        </Link>
                        <Divider />
                    </>
                }

                <Header>
                    <div>
                    {
                        (profile_img.length !== 0) &&
                        <img 
                            src={profile_img[0].secure_url} 
                            style={{
                                borderRadius: '10px',
                                margin: 'auto',
                                width: '100px',
                            }}
                        />
                    
                    }
                    <h2>{name}</h2>
                    </div>
                </Header>
                
                {
                    full_menu && active_full_menu.length > 1 &&
                    <HeadroomWrapper showSections={showSections} ref={sectionsWrapperRef}>
                        <Headroom 
                            disableInlineStyles 
                            wrapperStyle={{ paddingBottom: '2rem', paddingTop: '1rem'}}
                            pinStart={sectionsWrapperRef.current && (sectionsWrapperRef.current.offsetHeight + sectionsWrapperRef.current.scrollHeight) || 400}
                            onPin={() => setShowSections(true)}
                            onUnfix={() => setShowSections(false)}
                        >
                            <SectionMenu>
                            {
                                active_full_menu.map( (menu, i) =>  menu.section_title &&
                                <span 
                                    key={`sectionName${i}`} 
                                    onClick={() => scrollToRef(`secRef${i}`)}
                                    className="section-menu-item"
                                >
                                    {menu.section_title}
                                </span>
                                )
                            }
                            </SectionMenu>
                        </Headroom>
                    </HeadroomWrapper>
                }
                
                    
                <div>
                {
                    full_menu && active_full_menu.map( (menu, i) => <div
                        key={`fullStoreInfo${i}`} 
                        ref={(el) => {
                            sectionRefs.current[`secRef${i}`] = el
                        }}
                    >
                        <FullMenuCard 
                            menuSection={menu}
                            addItem={ addItem }
                            wa_orders ={wa_orders}
                        />
                    </div>
                    )
                }
                </div>
                {
                    wa_orders && 
                    <FinalOrder>
                        <Modal
                            isOpen={showOrderList} 
                            closeModal={closeModal}
                            full={true}
                        >
                            <h2>Tu pedido</h2>
                            <div className="order-details">
                                {
                                    order.map( (o, i) => <div className="order-food-wrapper" key={i}>
                                        <div className="order-food-name">
                                            <span>{o.qty}</span>
                                            <span>{o.food_name}{o.price[0].price_description && <small> ({o.price[0].price_description})</small>}</span>
                                            <span>${o.price[0].price_value * o.qty}</span>
                                        </div>
                                        <div><small>{o.special_request}</small></div>                            
                                    </div>)
                                }

                                <p className="order-total">
                                    <strong> Total Estimado ${
                                        order.reduce((a,b) => {
                                            return a + b.price[0].price_value * b.qty
                                        }, 0)
                                    }</strong>
                                    {/* <br/>
                                    <small>(Algunos establecimentos tienen cargo extras)</small> */}
                                </p>
                            </div>
                            <AskUserInfo user={user} handleOnChange={handleUserDetailsOnChange}/>
                            <div className="order-btn-wrapper">
                                <a 
                                    className="button whatsapp"
                                    href={`https://wa.me/52${use_whatsapp}/?text=${encodeOrder()}`}
                                    // href={`https://wa.me/16467213974/?text=${encodeOrder()}`}
                                >
                                    Enviar Pedido
                                </a>
                            </div>
                        </Modal>
                    
                        {
                            // order.length !== 0 &&
                            transitions.map(({ item, key, props }) =>
                                item && <animated.div key={key} style={props}>
                                    <WhatsappBtn
                                        onClick={() => setShowOrderList(true)}
                                    >
                                        Revisar y Enviar
                                    </WhatsappBtn>
                                </animated.div>
                            )
                        }
                    </FinalOrder>
                }

                <div style={{ textAlign: 'left', padding: '0 1rem' }}>
                    <div style={{ textAlign: 'left', marginBottom: '0', background: '#fff', padding: '0.5rem 1rem', borderRadius: '10px 10px 0 0', borderTop: '1px solid #e6e7e8' }}>
                        <ShowLocation location={location}>
                            {location.address}
                        </ShowLocation>
                    </div>
                    <Phone style={{ marginBottom: '0' }}>
                        <Icon size={'20px'}><img src="/icons/phone.svg" alt="tel" /></Icon>
                        <a href={`tel:+52${phone[0].number}`}>&nbsp;Llama al <strong>{phone[0].number}</strong></a>

                    </Phone>
                    <div style={{ textAlign: 'left', marginBottom: '1.5rem', borderBottom: '1px solid #e6e7e8', background:'#fff', padding: '1rem', borderRadius: '0 0 10px 10px' }}>
                        {
                            use_whatsapp && 
                            <>
                                <Icon size={'20px'}><img src="/icons/whatsapp.svg" alt="whatsapp" /></Icon>
                                <a 
                                    href={`https://wa.me/52${use_whatsapp}?text=Hola%2C%20v%C3%AD%20tu%20men%C3%BA%20en%20Comida%20God%C3%ADn%2C%20`}
                                    target="_blank"
                                >&nbsp;Envía un <strong>Whatsapp</strong></a>
                            </>
                        }
                    </div>
                </div>

                <ShareButtons 
                    url={`/${slug}`} 
                    text={`Te invitamos a ver nuestro menú. Esperamos verte pronto por aquí.`} 
                    media={has_images}
                />

                <PowerBy>
                    <div>Menú digital creado por</div>
                    <div>
                        <Link href="/">
                            <a>
                                <img src="/icons/comidagodin-tie.svg" alt="Comida Godín" />
                            </a>
                        </Link>
                    </div>
                </PowerBy>
            </Container>

            <style jsx global>{`
                body {
                    background: ${full_menu ? full_menu.customized.page_background_color : ''};
                    transition: background 0.3s;
                }
                
                main {
                    margin-top: 1rem;
                }
                
                .preview-header {
                    background: ${full_menu ? full_menu.customized.title_background_color : ''} !important;
                    color: ${full_menu ? full_menu.customized.title_text_color : ''} !important;
                }

                .preview-section, .dots {
                    background: ${full_menu ? full_menu.customized.items_background_color : ''} !important;
                    color: ${full_menu ? full_menu.customized.items_text_color : ''} !important;
                }
            `}</style>
        </>
    )
}
export default FullStoreInfo;

const Container = styled.div`
    max-width: 600px;
    margin: auto;
    padding: 0 1rem;

    .button {
        margin: 0.5rem;
    }

    .message {
        font-size: 0.75rem;
        margin-top: 1rem;
    }
`;

const Header = styled.div`
    text-align: center;
    margin-bottom: 2rem;
    font-size: 1.5rem;
`;

const Phone = styled.div`
    margin-bottom: 1rem;
    background: #fff;
    margin-left: -1rem;
    margin-right: -1rem;
    padding: 0.6rem 1rem 0.6rem 2rem;
    border-radius:10px 10px 2px 2px;
    text-align: left;
    position: relative;
    border: 1px solid #e6e7e8;

    &:before, &:after {
        content: "";
        position: absolute;
        z-index: -1;
        width: 1rem;
        height: 1rem;
        background: #e6e7e8;
        border-radius: 0 0 0 0.5rem;
        left: 0;
        bottom: -0.4rem;
    }
    &:after {
        left: initial;
        right: 0;
        border-radius: 0 0 0.5rem 0;
    }
`;

const PowerBy = styled.div`
    text-align: center;
    font-size: 0.75rem;
    margin-bottom: 3rem;
    img {
        width: 90px;
    }
`;

const SlideUp = keyframes`
    from {
        translate(-50%, 100%);
    }

    to {
        translate(-50%, 0%);
    }
`;

const WhatsappBtn = styled.button`
    background: #fbdb6d url('/icons/whatsapp.svg') no-repeat;
    color: #7a1a7b;
    box-shadow: 0 -2px 5px -3px rgba(0,0,0,0.3);
    background-size: 40px;
    background-position: 0.5rem 0.7em;
    background-repeat: no-repeat;
    padding: 1.5rem 1rem 2rem 56px;
    /* position: fixed;
    z-index: 1;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%); */
    border-radius: 30px 30px 0 0;
    line-height: 1;
    width: 195px;
    /* animation: ${SlideUp} 0.5s linear */
`;

const SectionMenu =  styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    font-size: 0.8rem;

    .headroom--unpinned &, .headroom--pinned & {
        flex-wrap: nowrap;
        justify-content: flex-start;
        overflow: scroll;

        .section-menu-item {
            box-shadow: none;
        }
    }

    .section-menu-item {
        padding: 0.5rem 1rem;
        margin: 0.5rem 0.2rem;
        border-radius: 30px;
        box-shadow: 0 0 0 1px rgba(0,0,0,0.1);
        background: #fff;
        display: flex;
        align-items: center;
    }
`;

const slideDown = keyframes`
  from {
    transform: translateY(-100%);
  }

  to {
    transform: translateY(0%);
  }
`;

const HeadroomWrapper = styled.div`
    .headroom-wrapper {
        height: initial !important;
    }
    .headroom {
        background: #fff;
        border-bottom: 1px solid #e6e7e8;
        box-shadow: 0 0 10px 5px rgba(0,0,0,0.2);
        transition: none;
    }
    .headroom--unfixed {
        position: relative;
        transform: translateY(0%);
        background: none;
        border-bottom: 0;
        box-shadow: none;
    }
    .headroom--scrolled {
        /* transition: transform 200ms ease-in-out; */
    }
    .headroom--unpinned {
        position: fixed;
        transform: translateY(0%);
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        animation: ${ ({showSections}) => !showSections ? css`${slideDown} 0.5s linear` : 'none'};
    }
    .headroom--pinned {
        position: fixed;
        transform: translateY(0%);
        padding-left: 0.5rem;
        padding-right: 0.5rem;
    }
`;

const FinalOrder = styled.div`
    .order-details {
        background: #fbdb6d;
        margin-bottom: 3rem;
        padding: 0.5rem;
        margin-left: -2rem;
        margin-right: -2rem;
    }

    .order-food-wrapper {
        border-bottom: 1px solid #d0d056;
        padding: 1rem;

        &:last-of-type {
            border: 0;
        }
    }
    .order-food-name {
        display: flex;
        justify-content: space-between;
    }
    .order-total {
        border-top: 1px solid #efca19;
        padding-top: 1rem;
        box-shadow: 0px -10px 20px -12px rgba(0,0,0,0.2);
    }
    
    .order-btn-wrapper {
        position: sticky;
        bottom: -34px;
        left: 0;
        width: 100%;
        background: #fff;
        box-shadow: 0 -17px 19px -13px rgba(0,0,0,0.2);
    }

    .whatsapp {
        background-image: url('/icons/whatsapp.svg');
        background-size: 55px;
        background-position: 1rem center;
        background-repeat: no-repeat;
        padding: 1rem 1rem 1rem 35px;
        width: 100%;
        margin: 10px 0;
    }
`;
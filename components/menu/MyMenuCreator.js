import { useState, useEffect, useRef } from 'react'
import Router from 'next/router'
import styled from 'styled-components'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Joyride, { STATUS } from 'react-joyride'
import PreviewAccordion from './PreviewAccordion'
import AddPhotoInMenu from './AddPhotoInMenu'
import { Divider, Icon } from '../../styles/common'
import { CREATE_FULL_MENU, GET_STORE_BY_SLUG, UPDATE_FULL_MENU } from '../../queries'
import Loading from '../common/Loading'
import Error from '../common/Error'
// import ModalToUpgrade from '../common/ModalToUpgrade'
import Modal from '../common/Modal'

const blankPrice = {
    price_value: "",
    price_description: ""
}

const blankFood = {
    food_name: "",
    food_image: "",
    food_price: [],
    food_description: "",
    food_tags: [],
    active: true,
}

const blankSection = {
    section_title: "",
    section_description: "",
    section_footer: "",
    section_image: "",
    food_items: [],
    active: true,
};

const notCustomized = {
    page_background_color: "#fafafa",
    title_background_color: "#7a1a7b",
    title_text_color: "#ffffff",
    items_background_color: "#ffffff",
    items_text_color: "#333333",
}

const init_steps = [
    {
        content: <div><h2>Empecemos</h2><p>Ve llenando tu menú en cada paso y presiona <strong>Siguiente</strong></p><p>Puedes ver una vista previa en la derecha de tu pantalla.</p><i>Si estás en tu teléfono, puedes dar clic en el ícono del ojo que está abajo a la derecha.</i></div>,
        placement: 'center',
        target: 'body',
    },
    {
        target: '.step-1',
        content: <div><p>Comienza por el nombre de la sección de tu menú.</p><strong>Ejemplo: Desayunos, Bebidas, Postres, ...</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-2',
        content: <div><i>(Puede estar vacío)</i><p>Escribe aquí algo que quieras decir antes de listar tu comida.</p><strong>Ejemplo: Sólo disponible de Lunes a Viernes</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-3',
        content: <div><p>Escribe el nombre de tu comida o bebida.</p><strong>Ejemplo: Cochinita Pibil, Sushi, ...</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-4',
        content: <div><i>(Puede estar vacío)</i><p>Escribe una descripción de tu comida o bebida</p><strong>Ejemplo: Receta de la casa. Suculenta y tentadora carne de cordero.</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-5',
        content: <div><p>Escribe el precio de tu comida o bebida.</p></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-6',
        content: <div><i>(Puede estar vacío)</i><p>Escribe el tamaño o cantidad</p><strong>Ejemplos: Chico, Con Queso, 2/pers, 12 pzs, Extra</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-12',
        content: <div><i>(Puede estar vacío)</i><p>Escribe algo que quieras decir al final de tu sección.</p><strong>Ejemplo: Todos vienen con ensalada y café.</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-13',
        content: <div><p>Agrega una foto.</p><strong>Puedes mostrar aquí una foto de tu platillo más popular.</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-11',
        content: <div><p>Continúa agregando tu comida al final de esta guía.</p></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-15',
        content: <div><p>Este botón creará otra sección nueva.</p></div>,
        placementBeacon: 'right',
    },
    {
        content: <div><strong>Si necesitas ayuda, no dudes en contactarnos por el chat de messenger que se encuentra en esta pantalla.</strong></div>,
        placement: 'center',
        target: 'body',
    },
];

function MyMenuCreator({ slug, isEditor }) {
    const full_section = {...blankSection};
    full_section.food_items = [{...blankFood}];
    full_section.food_items[0].food_price = [{...blankPrice}];

    const { data: userData, loading: userLoading, error: userError } = useQuery(GET_STORE_BY_SLUG, {
        variables: { slug}
    });
    const [menuSections, setMenuSections] = useState([{...full_section}]);
    const [ previewOpen, togglePreview ] = useState(false)
    const [ hasMenu, setHasMenu ] = useState(false);
    const [ mobilePreview, setMobilePreview ] = useState(false)
    const [isModalPhotoOpen, toggleModalPhoto] = useState(false)
    const [steps, setSteps] = useState(init_steps)
    const [ runTour, setRunTour ] = useState(false)

    const myRefs= useRef({});
    const scrollToRef = (ref) => {
        window.scrollTo(0, myRefs.current[ref].offsetTop);
        if(mobilePreview) setMobilePreview(!mobilePreview);
    };

    const [ createFullMenu, {loading, error}] = useMutation(CREATE_FULL_MENU, { 
        variables: {
            skip: !userData,
            menu: menuSections,
            ownerId: (isEditor && userData) && userData.getStoreBySlug._id,
        },
        refetchQueries: [{query: GET_STORE_BY_SLUG, variables: { slug }}],
    })

    const [ updateFullMenu, {loading: updateMenuLoading, error: updateMenuError}] = useMutation(UPDATE_FULL_MENU, {
        variables: {
            skip: !userData,
            fullMenuId: (userData && userData.getStoreBySlug.full_menu) && userData.getStoreBySlug.full_menu._id,
            menu: menuSections,
            ownerId: (isEditor && userData) && userData.getStoreBySlug._id,
        },
        refetchQueries: [{query: GET_STORE_BY_SLUG, variables: { slug }}],
    })

    useEffect ( () => {
        if((userData && userData.getStoreBySlug.full_menu) && userData.getStoreBySlug.full_menu.menu.length) {
            setMenuSections(userData.getStoreBySlug.full_menu.menu)
            togglePreview(true)
            setHasMenu(true)
        }
    }, [userData])
    
    if(userLoading) return <Loading />
    if(userError) return <Error error={error} />

    const { rights, _id } = userData.getStoreBySlug;
    const has_rights = !rights.includes('FREE');
    let pics_allowed = 3;
    if(rights.includes('PLAN_LITE')) pics_allowed = 8;
    if(rights.includes('PLAN_PREMIUM'))  pics_allowed = 20;


    function openTour(e) {
        e.preventDefault();
        setRunTour(true);
    }

    function handleEndOfTour(data) {
        const { status, type } = data;
        const finishedStatuses = [STATUS.FINISHED, STATUS.SKIPPED];
        if (finishedStatuses.includes(status)) {
            setRunTour(false);
        }
    }

    function handleMobilePreview() {
        setMobilePreview(!mobilePreview);
    }

    function handleOnChange(e, i) {
        e.persist();
        const { name, value } = e.target;
        
        const prevOne = menuSections;
        prevOne[i][name] = value;
        setMenuSections([...prevOne])
        if(!previewOpen) togglePreview(true)
    }

    function handleOnChangeFood(e, i, j) {
        e.persist();
        const { name, value } = e.target;
        const prevOne = menuSections;
        prevOne[i].food_items[j][name] = value;
        setMenuSections([...prevOne])
        if(!previewOpen) togglePreview(true)
        
    }

    function handleOnChangeTag(name, i, j) {
        const prevOne = menuSections;
        let tags = prevOne[i].food_items[j].food_tags;
        const already = tags.includes(name);
        
        if(!already) {
            tags.push(name);
        } else {
            prevOne[i].food_items[j].food_tags = tags.filter( tag => tag !== name);
        }
        setMenuSections([...prevOne])
        if(!previewOpen) togglePreview(true)
        
    }

    function handleOnChangePrice(e, i, j, k) {
        e.persist();
        const { name, value } = e.target;
        const prevOne = menuSections;
        prevOne[i].food_items[j]["food_price"][k][name] = value;
        setMenuSections([...prevOne])
        if(!previewOpen) togglePreview(true)
    }

    function addPhoto(pic, i, j) {
        const countedPhotos = calculatePhotos();
        const prevOne = menuSections;
        
        
        if(countedPhotos || prevOne[i].food_items[j]["food_image"]) {
            prevOne[i].food_items[j]["food_image"] = pic;        
            setMenuSections([...prevOne])
        }else{
            toggleModalPhoto(true)
        }
    };

    function addPhotoSection(pic, i) {
        const countedPhotos = calculatePhotos();
        const prevOne = menuSections;
        if(countedPhotos || prevOne[i].section_image) {
            prevOne[i].section_image = pic;        
            setMenuSections([...prevOne]);
        }else{
            toggleModalPhoto(true)
        }
    };

    function handleToggleModalPhoto() {
        toggleModalPhoto(false)
    }
    
    function calculatePhotos() {
        const arr = JSON.stringify(menuSections);
        const countPhotos = (arr.match(/https:\/\/res.cloudinary.com/g) || []).length;
        return countPhotos < pics_allowed;
    }

    function addPrice(i,j) {
        const menu = [...menuSections];
        menu[i].food_items[j].food_price = [ ...menu[i].food_items[j].food_price, {...blankPrice}];
        // console.log(menu[i].food_items[j].food_price);
        setMenuSections([...menu]);
    }

    function deletePrice(i,j,k) {
        const confirmDelete = window.confirm('¿Estás seguro que quieres borrar este precio?');
        if(confirmDelete){
            const menu = menuSections;
            menu[i].food_items[j].food_price = menu[i].food_items[j].food_price.filter( (p,i) => i !== k)
            setMenuSections([...menu]);
        }
        
    }

    function addFoodItem(i){
        const menu = [...menuSections];
        menu[i].food_items= [...menu[i].food_items, {...blankFood}];
        menu[i].food_items[menu[i].food_items.length - 1].food_price= [...menu[i].food_items[menu[i].food_items.length - 1].food_price, {...blankPrice}];
        menu[i].food_items[menu[i].food_items.length - 1].food_tags = [];
        setMenuSections([...menu])
    }

    function deleteFoodItem(i,j) {
        const confirmDelete = window.confirm('¿Estás seguro que quieres borrar esta comida o bebida?');
        if(confirmDelete){
            const menu = [...menuSections];
            menu[i].food_items = menu[i].food_items.filter( (f,i) => i !== j)
            setMenuSections([...menu]);
        }
    }

    function addSection(){
        const menu = [...menuSections];
        const newMenu = [...menu, {...full_section}]
        setMenuSections(newMenu)
    }

    function deleteSection(i) {
        const confirmDelete = window.confirm('¿Estás seguro que quieres borrar esta sección?');
        if(confirmDelete){
            let menu = [...menuSections];
            menu = menu.filter( (s,index) => index !== i)
            setMenuSections([...menu]);
        }
    }

    function swapUpSection(i) {
        let menu = menuSections;
        [menuSections[i], menuSections[i - 1]] = [menuSections[i - 1], menuSections[i]];
        setMenuSections([...menu]);
    }

    function swapDownSection(i) {
        let menu = menuSections;
        [menu[i], menu[i + 1]] = [menu[i + 1], menu[i]];
        setMenuSections([...menu]);
    }

    function swapUpFood(i,j) {
        let menu = menuSections;
        let currFood = menu[i].food_items;
        [currFood[j], currFood[j - 1]] = [currFood[j - 1], currFood[j]];
        setMenuSections([...menu]);
    }

    function swapDownFood(i,j) {
        let menu = menuSections;
        let currFood = menu[i].food_items;
        [currFood[j], currFood[j + 1]] = [currFood[j + 1], currFood[j]];
        setMenuSections([...menu]);
    }

    function changeStatusFood(i,j) {
        const menu = menuSections;
        if(menu[i].food_items[j].active === null) {
            menu[i].food_items[j].active = true;
        }
        menu[i].food_items[j].active = !menu[i].food_items[j].active;
        setMenuSections([...menu]);
    }

    function changeStatusSection(i) {
        const menu = menuSections;
        if(menu[i].active === null) {
            menu[i].active = true;
        }
        menu[i].active = !menu[i].active;
        setMenuSections([...menu]);
    }

    async function handleClickPublishMenu() {
        const res = await createFullMenu();
        Router.push(`/${slug}`)
    }
    async function handleClickUpdateMenu() {
        const res = await updateFullMenu();
        Router.push(`/${slug}`)
    }

    async function saveBeforeRedirectToPlans() {
        const res = hasMenu ? await updateFullMenu() : await createFullMenu();
        Router.push('/negocios/ver-planes'); 
    }

    const InputsGroups = menuSections.map((menuSection, i) => (
        <div key={`sectkey${i}`}>
            <FieldsetAndPreviewWrap>
                <FieldsetWrap>
                    <Fieldset mobilePreview={mobilePreview} disabled={menuSection.active === null ? false : !menuSection.active}>
                        <div className="section-title">
                            <input 
                                type="text" 
                                value={menuSection.section_title} 
                                name="section_title" 
                                placeholder="Nombre de la Sección"
                                onChange={(e) =>handleOnChange(e,i)} 
                                className="step-1"
                            />
                        </div>
                        <textarea 
                            name="section_description" 
                            rows="3" 
                            value={menuSection.section_description} 
                            placeholder="Mensaje al inicio (Opcional) "
                            onChange={(e) =>handleOnChange(e,i)}
                            className="step-2"
                        ></textarea>

                        <div className="food-items">
                            {
                                menuSection.food_items.map( (foodItem, j) => (
                                    <div 
                                        key={`foodkey${i}${j}`}
                                        className="food-item"
                                        ref={(el) => {
                                            
                                            myRefs.current[`foodRef${i}${j}`] = el
                                        }}
                                    >
                                        <fieldset className="food-name" disabled={foodItem.active === null ? false : !foodItem.active}>
                                            <div className="food-left">
                                                <input
                                                    type="text"
                                                    name="food_name"
                                                    value={menuSection.food_items[j].food_name}
                                                    placeholder="Alimento ó bebida"
                                                    onChange={(e) =>handleOnChangeFood(e,i,j)}
                                                    className="step-3"
                                                />
                                                <textarea 
                                                    name="food_description" 
                                                    rows="3" 
                                                    value={menuSection.food_items[j].food_description} 
                                                    placeholder="Descripción (Opcional)"
                                                    onChange={(e) =>handleOnChangeFood(e,i,j)}
                                                    className="step-4"
                                                ></textarea>
                                                <div>
                                                    {
                                                        menuSection.food_items[j].food_price.map( (price, k) => (
                                                            <div key={`priceKey${i}${j}${k}`} className="price-wrap">
                                                                <input
                                                                    type="text"
                                                                    name="price_value"
                                                                    value={menuSection.food_items[j].food_price[k].price_value}
                                                                    placeholder="$ Precio"
                                                                    onChange={(e) =>handleOnChangePrice(e,i,j,k)}
                                                                    className="price-value"
                                                                    className="step-5"
                                                                />
                                                                <input
                                                                    type="text"
                                                                    name="price_description"
                                                                    value={menuSection.food_items[j].food_price[k].price_description}
                                                                    placeholder="(Opcional)"
                                                                    onChange={(e) =>handleOnChangePrice(e,i,j,k)}
                                                                    className="price-desc"
                                                                    className="step-6"
                                                                />
                                                                {
                                                                    menuSection.food_items[j].food_price.length > 1 &&
                                                                    <span onClick={() => deletePrice(i,j,k)} style={{ width: '60px', marginRight: '-3.4rem', marginLeft: '1rem', padding: '0.5rem', zIndex: '0'  }}><img src="/icons/delete.svg" alt="borrar"/></span>
                                                                }
                                                                
                                                            </div>
                                                        ))
                                                    }
                                                    <ButtonAddMenu onClick={ () => addPrice(i,j)}>
                                                        <span className="icon"><img src="/icons/price.svg" alt="+"/></span>
                                                        <span className="step-7">Agrega otro Precio</span>
                                                    </ButtonAddMenu>
                                                    {
                                                        has_rights && 
                                                        <div className="tag-wrap" >
                                                            <div className="step-8">Selecciona si es:</div>
                                                                <span 
                                                                    className={`tag ${menuSection.food_items[j].food_tags.includes('Picoso') && 'selected'}`} 
                                                                    onClick={ () => has_rights && handleOnChangeTag("Picoso", i, j)}
                                                                >
                                                                    Picoso
                                                                </span>
                                                                
                                                                <span 
                                                                    className={`tag ${menuSection.food_items[j].food_tags.includes('Vegetariano') && 'selected'}`} 
                                                                    onClick={ () => has_rights && handleOnChangeTag("Vegetariano", i, j)}
                                                                >
                                                                    Vegetariano
                                                                </span>

                                                                <span
                                                                    className={`tag ${menuSection.food_items[j].food_tags.includes('Popular') && 'selected'}`}
                                                                    onClick={ () => has_rights && handleOnChangeTag("Popular", i, j)}
                                                                >
                                                                    Popular
                                                                </span>
                                                        </div>
                                                    }
                                                        
                                                </div>
                                            </div>
                                            <div className="step-9">
                                                {
                                                    has_rights &&
                                                    <AddPhotoInMenu addPhotoToParentState={addPhoto} i={i} j={j} hasImage={menuSection.food_items[j].food_image} storeId={ isEditor && _id } />
                                                    // :
                                                    // <ModalToUpgrade callback={saveBeforeRedirectToPlans} disabled={!has_rights} isPic={true}>
                                                    //     <div style={{ padding: '0.5rem', border: '1px solid #e6e7e8', borderRadius: '10px' }}>Foto</div>
                                                    // </ModalToUpgrade> 
                                                    
                                                }
                                            </div>
                                        </fieldset>
                                        {
                                            j > 0 &&
                                            <span onClick={() => swapUpFood(i,j)}><Icon><img src="/icons/arrow-up.svg" alt="Subir" /></Icon></span>
                                        }
                                        {
                                            j + 1 < menuSections[i].food_items.length &&
                                            <span onClick={() => swapDownFood(i,j)}><Icon><img src="/icons/arrow-down.svg" alt="Bajar" /></Icon></span>
                                        }
                                        <span onClick={() => changeStatusFood(i,j)}>
                                            {
                                               foodItem.active || foodItem.active === null
                                               ? <Icon><img src="/icons/no-see.svg" alt="Desactivar" /></Icon>
                                               : <Icon><img src="/icons/see.svg" alt="Activar" /></Icon>
                                            }
                                        </span>
                                        <div className="step-10">
                                            <DeleteSectionAndFood onClick={() => deleteFoodItem(i,j)}>Borra Este Alimento ó Bebida</DeleteSectionAndFood>
                                        </div>
                                    </div>
                                ))
                            }

                            {
                                <ButtonAddMenu onClick={() => addFoodItem(i)}> 
                                    <span className="icon"><img src="/icons/add-menu.svg" alt="+"/></span>
                                    <span className="step-11">Agrega otro Alimento ó bebida</span>
                                </ButtonAddMenu>
                            }
                    
                        </div>

                        <textarea 
                            name="section_footer" 
                            rows="3"
                            placeholder="Mensaje al final (Opcional)"
                            value={menuSection.section_footer} 
                            onChange={(e) =>handleOnChange(e,i)}
                            className="step-12"
                        ></textarea>

                        <div className="step-13"><AddPhotoInMenu addPhotoToParentState={addPhotoSection} i={i} hasImage={menuSection.section_image} storeId={ isEditor && _id } /></div>
                        <div className="step-14">
                        {
                            i > 0 &&
                            <span onClick={() => swapUpSection(i)}><Icon><img src="/icons/arrow-up.svg" alt="Subir" /></Icon></span>
                        }
                        {
                            i + 1 < menuSections.length &&
                            <span onClick={() => swapDownSection(i)}><Icon><img src="/icons/arrow-down.svg" alt="Bajar" /></Icon></span>
                        }
                            <span onClick={() => changeStatusSection(i)}>
                                {
                                    menuSection.active || menuSection.active === null
                                    ? <Icon><img src="/icons/no-see.svg" alt="Desactivar" /></Icon>
                                    : <Icon><img src="/icons/see.svg" alt="Activar" /></Icon>
                                }
                            </span>
                            <DeleteSectionAndFood onClick={() => deleteSection(i)}>Borra esta sección</DeleteSectionAndFood>
                        </div>
                    </Fieldset>
                </FieldsetWrap>
                <PreviewMenuWrap open={previewOpen} mobilePreview={mobilePreview}>
                    <PreviewMenu>
                        <PreviewAccordion menuSection={menuSection} customizeInputs={notCustomized} scrollToRef={scrollToRef} indexRef={i} />
                    </PreviewMenu>
                </PreviewMenuWrap>
            </FieldsetAndPreviewWrap>
            <Divider/>
        </div>
    ));


    return (
        <GridMenuCreator>
            <Joyride
                steps={steps}
                styles={{
                    options: {
                      overlayColor: 'rgba(0, 0, 0, 0.5)',
                      primaryColor: '#7a1a7b',
                      width: 400,
                      zIndex: `${mobilePreview ? '-1' : '1'}`,
                    }
                }}
                continuous= {true}
                locale= {{ back: 'Regresar', close: 'Cerrar', last: 'Fin', next: 'Siguiente', skip: 'Cerrar' }}
                showProgress={true}
                run={runTour}
                spotlightClicks={true}
                callback={handleEndOfTour}
            />
            <div>
                <p>
                    <button 
                        className="secondary" 
                        onClick={openTour} 
                        disabled={runTour}
                    >Clic aquí para mostrar guía</button>
                </p>
                
                <See onClick={handleMobilePreview} mobilePreview={mobilePreview}></See>

                {InputsGroups}

                <Modal isOpen={isModalPhotoOpen} closeModal={handleToggleModalPhoto}>
                    <span style={{fontSize: '2rem'}}>😔</span>
                    <div style={{ fontSize: '1.5rem' }}> No se pudo agregar </div>
                    <p>Con éste plan sólo puedes tener <strong>{pics_allowed} foto{pics_allowed > 1 && 's'}</strong> en tu menú.</p>
                    <p>Agrega más fotos por sólo $35 pesos al mes.</p>
                    <button className="secondary" onClick={saveBeforeRedirectToPlans}>Ver más información</button>
                </Modal>

                <ButtonAddMenu section onClick={addSection}> 
                    <span className="icon"><img src="/icons/add-menu.svg" alt="+"/></span>
                    <span className="step-15">Agrega otra sección</span>
                </ButtonAddMenu>

                <button 
                    className="secondary"
                    onClick={hasMenu ? handleClickUpdateMenu : handleClickPublishMenu}
                >
                    Guardar Menú
                </button>

            </div>
        </GridMenuCreator>
    )
}

export default MyMenuCreator;

const GridMenuCreator = styled.div`
    padding: 0 1rem;

    button.react-joyride__beacon[type="button"]:after {
        content: "?";
        color: #fff;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        font-family: Helvetica;
        font-size: 0.8rem;
    }
`;

const ButtonAddMenu = styled.button`
    background: ${ ({section}) => section? '#333' : '#fafafa'};
    padding: 0.3rem 1rem 0.3rem 0.5rem;
    border: 1px solid #e6e7e8;
    font-family: Helvetica, sans-serif;
    font-size: 0.8rem;
    color: ${ ({section}) => section? '#fff' : '#333'};
    margin: 0.5rem 0.5rem 1rem;
    
    span {
        display: inline-block;
        vertical-align: middle;
    }
    .icon {
        width: 20px;
        margin-right: 0.5rem;
    }
`

const See = styled.span`
    width: 50px;
    height: 50px;
    background: ${ ({mobilePreview}) => mobilePreview ? `url('/icons/no-see.svg')` : `url('/icons/see.svg')`}, #fff;
    background-size: 35px;
    background-position: center;
    background-repeat: no-repeat;
    border-radius: 30px;
    position: fixed;
    right: 2rem;
    bottom: 100px;
    box-shadow: 0 0 10px 2px rgba(0,0,0,0.2);
    z-index: 1;
    cursor: pointer;

    ${({mobilePreview}) => mobilePreview && (
        `&:after {
            content: "";
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-bottom: 5px solid black;
            position: absolute;
            top: 0;
            left: 50%;
            transform: translateX(-50%)
        }
        
        &:before {
            content: "";
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 5px solid black;
            position: absolute;
            bottom: 0;
            left: 50%;
            transform: translateX(-50%)
        }`
    )}

    @media all and (min-width:640px) {
        display: none;
    }
`;

const FieldsetAndPreviewWrap = styled.div`
    display: flex;
    justify-content: center;
`;

const FieldsetWrap = styled.div`
    overflow: hidden;
    flex: 100%;
    width: 0;

    @media all and (min-width:640px) {
        flex: 1 1 100%;
        width: auto;
    }
`;

const Fieldset = styled.fieldset`
    border: 1px solid #e6e7e8;
    border-radius: 10px;
    max-width: 400px;
    width: 100%;
    padding: 0.5rem;
    margin-bottom: 1.5rem;
    background-color: #e8e8e8;

    .section-title {
        /* background-color: ${notCustomized.title_background_color}; */
        /* color: ${notCustomized.title_text_color}; */
        color: #7a1a7b;
        border-radius: 10px 10px 0 0;
    }

    input, textarea {
        /* background-color: rgba(0,0,0,0.05); */
        background: #fff;
        border: 1px solid #e6e7e8;
        color: inherit;
        border-radius: 10px;
        font-size: 1rem;
        width: 100%;
        padding: 0.5rem;
        font-family: Helvetica, sans-serif;
        min-width: initial;
        margin: 0 0 0.5rem 0;
    }
    .price-wrap {
        display: flex;
    }
    
    .tag-wrap {
        font-size: 0.8rem;
        margin: 1rem 0;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-evenly;
        cursor: pointer;
    }
    .tag {
        margin-left: 0.5rem;
        padding: 0.5rem;
        background: #fff;
        border: 1px solid #e6e7e8;
        border-radius: 30px;
        margin-bottom: 0.5rem;
        
        &.selected {
            background: #7a1a7b;
            color: #fff;
        }
    }

    input.price-value {
        width: 5rem;
    }

    .food-name {
        display: flex;
        padding: 1.5rem 0 0;
        border: 0;

        .food-left {
            flex: 1 0 68%;
            padding-right: 0.5rem;
        }

        &:disabled {
            opacity: 0.3;
        }
    }

    .food-items {
        margin: 0 -0.5rem 0.5rem -0.5rem;
        padding: 0.5rem;
        background: #e8e8e8;
    }

    .food-item {
        border-radius: 10px;
        padding: 2px;
        background: #fafafa;
        margin-bottom: 0.7rem;

        /* &:nth-child(odd) {
            background: #fafafa;
        } */
    }
`;

const PreviewMenuWrap = styled.div`
    flex: ${ ({mobilePreview}) => mobilePreview ? '0 0 320px' : '0'};
    overflow-x: hidden;
    overflow-y: scroll;
    transition: flex 0.5s;
    position: sticky;
    align-self: flex-start;
    top: 0;
    width: 0;
    padding-top: 60px;

    @media all and (min-width:640px) {
        flex: ${ ({open}) => open ? '0 0 320px' : '0 0 0'};
        width: auto;
    }
`;

const PreviewMenu = styled.div`
    width: 320px;
    padding: 1rem;
    font-size: 0.8rem;

    .preview-title {
        background: #ffffff;
        border-radius: 20px;
        border: 1px solid #e6e7e8;
        margin-bottom: 2rem;
        display: inline-block;
        padding: 0.5rem;
        color: #999;
    }
`

const DeleteSectionAndFood = styled.span`
    padding: 0.8rem;
    font-size: 0.6rem;
    margin: 1rem auto 0;
    display: inline-block;
    cursor: pointer;
`;
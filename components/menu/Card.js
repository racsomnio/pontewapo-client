import Link from 'next/link'
import styled from 'styled-components';
import { Icon } from '../../styles/common'

const Card = ({data, meal}) => {
    const convertToMins = (m) => {
        return Math.ceil((m * 1.3) / 60);
    }

    return (
        <Item>
            <Link href="/[meal]/[menuId]" as={`/${meal}/${data._id}`}>
                <a>
                    <CardContent>
                        <Img>
                            
                            {/* <img 
                                src={ 
                                    (data.menu_image.length) 
                                    ? `${data.menu_image[0].secure_url}` 
                                    : `${process.env.PUBLIC_URL}/no-image.png`
                                } 
                                alt="{data.owner.name}"
                            /> */}
                            <span className="store-img" style={{
                                backgroundImage: `url(${ data.menu_image.length && data.menu_image[0].secure_url})`
                            }}></span>
                        </Img>
                        <div className="card-title" >{data.owner.name}</div>
                        <span><small>Pregunta por:</small> <small style={{ background:'#fbdb6d', borderRadius:'10px', padding: '0.2rem 1rem', fontWeight:'bold'}}>{ data.owner.speciality[0] }</small></span>
                        <div style={{ margin: '0.3rem'}}><small>[ {data.owner.typeOfBusiness} ]. Comida {data.owner.food_type.type} </small> </div>
                        <span className="card-dis">Aprox. {convertToMins(data.distance)} mins. &nbsp;</span>
                        <span className="card-dis">
                        {
                            data.owner.delivery && <Icon size={'20px'}><img src ="/icons/delivery.svg" alt="entrega"/></Icon>
                        }
                        {
                            data.owner.takeaway && <Icon size={'20px'}><img src ="/icons/takeaway.svg" alt="llevar"/></Icon>
                        }
                        {
                            data.owner.only_cash && <Icon size={'20px'}><img src ="/icons/cash.svg" alt="cash"/></Icon>
                        }
                        {
                            data.owner.card && <Icon size={'20px'}><img src ="/icons/card.svg" alt="cash"/></Icon>
                        }
                        </span>
                    </CardContent>
                </a>
            </Link>
        </Item>
    )
}

const Item = styled.div`
    position: relative;
    margin-bottom: 2rem;
`;

const CardContent = styled.div`
    bottom: 1rem;
    right: 1rem;
    text-align: center;

    .card-title {
        text-transform: capitalize;
        font-family: 'Baloo 2',cursive;
        position: relative;
        padding-top: 4px;
        font-size:1.2rem;
    }
    .store-img {
        width: 250px;
        height: 250px;
        background: #f2f2f2;
        display: block;
        margin: auto;
        border-radius: 50%;
        box-shadow: -1px 0 #f2f2f2;
        background-size: cover;
    }

    .card-dis {
        font-size: 0.7rem;
        vertical-align: middle;
    }
`;

const Img = styled.div`
    position: relative;

    img {
        border-radius: 0;

        @media all and (min-width: 410px) {
            border-radius: 10px;
        }
    }
`;

export default Card

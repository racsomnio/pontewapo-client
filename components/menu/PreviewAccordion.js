import { useRef, useState, useEffect } from 'react'
import styled from 'styled-components'

function PreviewAccordion({menuSection, customizeInputs, scrollToRef, indexRef}) {
    return (
        <PreviewSection customizeInputs={customizeInputs}>
            <div className="preview-section">
                <div className="preview-header">
                    {menuSection.section_title}
                </div>
                <div 
                    className="accordion"
                >
                    <div className="preview-desc">
                        {menuSection.section_description}
                    </div>
                    <div className="preview-food">
                        {menuSection.food_items.map( (f,i) => (
                            <div 
                                key={`f.food_name${i}`} 
                                className="preview-food-wrap"
                                onClick={() => scrollToRef(`foodRef${indexRef}${i}`)}
                            >
                                <div className="preview-food-info">
                                    <div className="preview-food-name">
                                        {f.food_name}
                                    </div>
                                    <div>
                                        {f.food_description}
                                    </div>
                                    <div className="preview-prices">
                                    {
                                        (f.food_image || f.food_price.length > 1) &&
                                        f.food_price.map( (p, j) => (
                                            <div
                                                key={`prevPriceKey${i}${j}`} 
                                                style={{ paddingTop: '0.5rem', }}
                                                className="preview-prices-item"
                                            >
                                                
                                                <span>{f.food_price[j].price_value && '$'}{f.food_price[j].price_value}</span>
                                                <span className="preview-prices-desc">&nbsp;{f.food_price[j].price_description}</span>
                                            </div> 
                                        ))
                                    }
                                    </div>
                                    <div className="preview-tag-wrap">   
                                        {
                                            f.food_tags.map( tag => (
                                                <span key={tag} className="preview-tag">{tag}</span>
                                            ))
                                        }
                                    </div>
                                </div>
                                { f.food_image ?
                                    <Pic src={f.food_image}>
                                        
                                    </Pic> :
                                    f.food_price.length === 1 &&
                                        <div>
                                            {
                                                f.food_price.map( (p, j) => (
                                                    <div
                                                        key={`prevPriceKey${i}${j}`} 
                                                    >
                                                        <span>{f.food_price[j].price_value && '$'}{f.food_price[j].price_value}</span>
                                                        <span>&nbsp;{f.food_price[j].price_description}</span>
                                                    </div> 
                                                ))
                                            }
                                        </div> 
                                }
                                
                            </div>
                        ))}
                    </div>
                    <div className="preview-desc">
                        {menuSection.section_footer}
                    </div>
                    {
                    menuSection.section_image &&
                        <div className="preview-section-img">
                            <img src={menuSection.section_image}  alt={menuSection.section_title} />
                        </div>
                    }
                </div>
            </div>
        </PreviewSection>
    )
}

export default PreviewAccordion


const PreviewSection = styled.div`
    background: ${({customizeInputs}) => customizeInputs.items_background_color};
    color: ${({customizeInputs}) => customizeInputs.items_text_color};
    border-radius: 10px;
    margin-bottom: 3.5rem;
    box-shadow: 0 0 0 1px rgba(0,0,0,0.15);
    position: relative;
        .preview-section {
            border-radius: inherit;
        }

        .preview-header {
            background: ${({customizeInputs}) => customizeInputs.title_background_color};
            color: ${({customizeInputs}) => customizeInputs.title_text_color};
            border-radius: 30px ;
            font-weight: bolder;
            padding: 0.8rem 1.5rem;
            display: inline-block;
            margin-top: -1rem;
            vertical-align: middle;
        }

        .preview-desc {
            padding: 0.5rem;
            white-space: pre-wrap;
        }

        .preview-section-img {
            padding: 0.5rem;

            img {
                border-radius: 5px;
            }
        }

        .preview-food-wrap {
            display: flex;
            padding: 0.5rem;
            justify-content: space-between;
            text-align: left;
            position: relative;
            cursor: pointer;
            transition: background 0.3s;

            &:hover {
                background: #fbdb6d;
            }

            &:after{
                content:"";
                width: 80%;
                border-bottom: 1px dotted rgba(0,0,0,0.1); 
                position: absolute;
                bottom: 0;
                left: 50%;
                transform: translateX(-50%);
            }

            .preview-food-info {
                flex: 3;
                padding-right: 0.5rem;
            }
        }

        .preview-food-name {
            font-weight: bolder;
        }
    
        .preview-prices {
            display: flex;
            /* text-align: center; */
            align-items: flex-end;
            justify-content: flex-start;

            .preview-prices-item {
                padding-right: 1rem;
                
                &.left {
                    padding-top: 0.5rem;
                }
            }

            .preview-prices-desc {
                font-size: 0.75rem;
            }
        }

        .preview-tag-wrap {
            margin-top: 0.5rem;
            display: flex;
            flex-wrap: wrap;
            justify-content: flex-start;

            .preview-tag {
                font-size: 0.6rem;
                padding: 0.4rem;
                border-radius: 30px;
                border: 1px solid #e6e7e8;
                margin-right: 0.5rem;
                background: rgba(255,255,255, 0.5)
            }
        }

    .accordion-btn {
        width: 30px;
        height: 30px;
        position: absolute;
        top: 0;
        right: 0.5rem;
        cursor: pointer;

        &:before {
            content: "";
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;
            border-top: 7px solid #333;
            top: 50%;
            position: absolute;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    }

    .accordion {
        overflow: hidden;
        transition: all 0.5s;
    }
`;


const Pic = styled.div`
    background-image: ${ ({src}) => src ? `url(${src})`: 'none'};
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    border-radius: 40px;
    flex: 1;
`;
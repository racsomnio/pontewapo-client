import { useRouter } from 'next/router'
import { useQuery } from '@apollo/react-hooks'
import { GET_MENU } from '../../queries'
import Loading from '../common/Loading'
import Error from '../common/Error'
import dynamic from 'next/dynamic'
const UpdateMenu = dynamic(
    () => import('../menu/UpdateMenu'),
    { ssr: false }
)

function GetMenu() {
    const router = useRouter()
    const { menuId } = router.query;

    const {loading, error, data } = useQuery(GET_MENU,{
        variables: { _id: menuId }
    })

    if(loading) return <Loading/>
    if(error) return <Error error={error}/>

    return (
        <div>
            <h1>Editar Menú</h1>
            <UpdateMenu data={data.getMenu}/>
        </div>
    )
}

export default GetMenu

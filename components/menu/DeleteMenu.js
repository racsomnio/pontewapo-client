import Router from 'next/router'
import { useMutation } from "@apollo/react-hooks"
import { DELETE_MENU, GET_MY_MENUS } from '../../queries';
import { ButtonLike, Icon } from '../../styles/common';

const DeleteMenu = ({ menuId, ownerId }) => {
    const [deleteMenu] = useMutation(DELETE_MENU, {
        variables: { menuId, ownerId },
        refetchQueries: [
            {
                query: GET_MY_MENUS
            }
        ],
        awaitRefetchQueries: true,
      });

    const handleClick = async () => {
        const confirmDelete = window.confirm('¿Estas seguro?');
        if(confirmDelete){
            await deleteMenu()
            Router.push('/negocios/ver-menus')
        }
    }
    
    return (

        <ButtonLike onClick={ handleClick }>
            <Icon size={'12px'}><img src="/icons/delete.svg"/></Icon>
            &nbsp;Borrar
        </ButtonLike>
    )
}

export default DeleteMenu

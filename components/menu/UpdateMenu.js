import { useState } from 'react'
import Router from 'next/router'
import { useMutation } from '@apollo/react-hooks'
import { GET_MENU, UPDATE_MENU, GET_MENUS } from '../../queries'

import styled from 'styled-components';
import DayItem from './DayItem';
import { Icon } from '../../styles/common';
import AddPhoto from './AddPhoto';
import { swap_it } from '../../utils';

//Editor
import { EditorStyles } from '../../styles/EditorStyles';
import { convertToRaw, ContentState, EditorState, convertFromHTML } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';

const UpdateMenu = ({ data }) => {
    const { _id, meal, repeat, html_menu, menu_image } = data;
    const coordinates =localStorage.getItem('coords');

    const [ menu, setMenu] = useState({
        _id,
        meal: meal || 'lunch',
        repeat: repeat || [],
        html_menu: html_menu || '',
        menu_image: menu_image || [],
    });

    const fromHtml = convertFromHTML(html_menu);

    if(fromHtml.contentBlocks){
        const content = ContentState.createFromBlockArray(
            fromHtml.contentBlocks,
            fromHtml.entityMap
        );
        var edSt =  EditorState.createWithContent(content);
    } else {
        var edSt = EditorState.createEmpty();
    }
    
    const [ editorState, setEditorState ] = useState(edSt);

    function onEditorStateChange(es) {
        setEditorState(es)
        const toHTML = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        
        setMenu(prevState => ({
            ...prevState,
            html_menu: toHTML
        }));
    }

    function addPhoto(pic) {
        setMenu(prevState => ({
            ...prevState,
            menu_image: [ pic, ...prevState.menu_image ]
        }));
    };

    function handleChange(e) {
        const { name, value } = e.target;
        setMenu(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function handleDaysClick(e) {
        if(e.target.className.includes(' day')){
            const dayNum = {
                Lun: 1,
                Mar: 2,
                Mie: 3,
                Jue: 4,
                Vie: 5,
                Sab: 6,
                Dom: 0
            }
            const { innerText } = e.target;

            let arr = menu.repeat;
            const dayExists = arr.includes(dayNum[innerText]);
            
            if(!dayExists){
                arr = [...arr, dayNum[innerText]]
            }else{
                arr = arr.filter((i) => { return i !== dayNum[innerText] })
            }

            setMenu(prevState => ({
                ...prevState,
                repeat: arr
            }))
        }
    }

    async function handleSubmit(callback) {
        await callback();
        const swap_menu = Object.keys(swap_it).filter((k) => {
            if(swap_it[k] == menu.meal) return k
        });
        Router.push('/[meal]/[menuId]', `/${swap_menu}/${_id}`);
    }

    function isSelected(numDay) {
        return repeat.includes(numDay);
    }

    const [ createMenu, {loading} ] = useMutation(UPDATE_MENU,{
        variables: menu,
        refetchQueries: [
            { 
                query: GET_MENU, 
                variables: { _id } 
            },
            {
                query: GET_MENUS,
                variables: {coordinates, meal: menu.meal}
            },
        ]
    })
    
    return (
        <Fieldset>
            <Message>Elije una foto para este menú</Message>
            <div style={{ margin: '1rem 0 3rem'}}>
                <AddPhoto addPhotoToParentState={ addPhoto } hasImage={menu.menu_image}/>
            </div>

            <Week>
                <Message>Seleccciona el día o los días que este menú está disponible</Message>
                <div className="days" onClick={handleDaysClick}>
                    <DayItem day="Lun" val={1} selected={() => isSelected(1)} />
                    <DayItem day="Mar" val={2} selected={() => isSelected(2)} />
                    <DayItem day="Mie" val={3} selected={() => isSelected(3)} />
                    <DayItem day="Jue" val={4} selected={() => isSelected(4)} />
                    <DayItem day="Vie" val={5} selected={() => isSelected(5)} />
                    <DayItem day="Sab" val={6} selected={() => isSelected(6)} />
                    <DayItem day="Dom" val={0} selected={() => isSelected(0)} />
                </div>
            </Week>

            <EditorStyles>
                <Message>Edita tu menú.</Message>
                <Editor 
                    // initialContentState={contentState}
                    // onContentStateChange={ onContentStateChange }
                    editorState={editorState}
                    onEditorStateChange={ onEditorStateChange }
                    localization={{
                        locale: 'es',
                    }}
                    placeholder={`Escribre / Copia y pega aquí`}
                    toolbar={{
                        options: ['emoji', 'blockType', 'inline', 'history'],
                        inline: { options: ['bold', 'underline']},
                        blockType: {inDropdown: false, options: ['H2', 'Normal'], className: 'editor-h2',},
                        emoji: { 
                            emojis: [
                                '☕','🍕','🌮','🍔','🌭', '🥟', '🌯','🥪','🥐', '🥞','🥖', '🍞',  '🍳','🍖', '🍗', '🥩', '🥓','🍣', '🥣','🍲', '🍤','🍛', '🥗','🍜', '🍝', '🍱', '🍿','🍟','🥡','🍚', '🍦', '🍩', '🍪', '🧁', '🍫', '🍬', '🍮', '🍰', '🎂', 
                                '🌶', '🌽','🍄','🍅', '🍇', '🍉', '🍊', '🍋', '🍌', '🍍', '🍎', '🍑', '🍓', '🥭', '🍐', '🥝', '🍈', '🥑', '🥜', '🥥', '🥕', '🍠', '🥦', '🧇',
                                '🐑','🐄','🐖','🐥','🐐', '🐠', '🦀', '🦞', '🌿', '🐛', '🦗', '🌹',
                                '🥤', '🍽', '🍷', '🍺', '🍻', '🍹', '🍾','🍸','🥂', '🥃', '🧃',
                                '👇', '🤘', '🖐', '👌', '👍', '👎', '🤙', '👊', '👏', '🙏','💪', '👈', '👉', '👆','💘','☎', '💵', '💳',
                                '🌙', '🌞','🌨', '🌩', '🔥', '🧊',
                                '🎃','⛄', '🎅','🎄','💃','👑','🎉', '🎈', '🎁','⚽', '🏀', '🏈', '⚾','🎾', '🥊', '🎳','🏆','🏁', '🎵', '🎷', '💰', '🖊', 
                                '✅','🇲🇽','🇦🇷','🇺🇾','🇨🇴', '🇵🇪', '🇰🇷','🇯🇵','🇮🇹','🇮🇳','🇪🇦', '🇩🇪', '🇨🇺', '🇧🇷',
                                '🤯','😀', '😁', '😂', '😉', '😋', '😎', '😍', '😴', '😌', '🤓', '😷','😭','😱','😰', '🤤','😪', '🥵', '🥶','🥴','🥳', '🥺', '🥱', 
                            ],
                        },
                        fontSize: {options: [12, 16],},
                        list: { options: ['unordered','ordered' ]},
                    }}
                />
            </EditorStyles>
            
            <button 
            style={{ marginTop: '1rem' }}
                onClick={() => handleSubmit(createMenu)}
            >
                Actualizar Menú
            </button>
                
        </Fieldset>
    )
}

const Fieldset = styled.fieldset`
    border-radius: 3rem;
    border:0;
    border-top: 4px solid #f2f2f2;
    padding: 1.5rem 0;
    margin-top: 2rem;
    max-width: 600px;
    margin: auto;
    background: #fff;

    @media all and (min-width: 768px){
        padding: 1.5rem 4rem 3rem;
        border: 1px solid #e6e7e8;
        min-width: 500px;
    }

    legend {
        padding-left: 1rem;
        padding-right: 1rem;
    }
`;

const Message = styled.p`
    font-size: 0.8rem;
    margin-bottom: 0.5rem;
`;

const Week = styled.div`
    text-align: center;
    margin: 0 auto 2.5rem;
    max-width: 350px;

    .days{
        background: #f2f2f2;
        border-radius: 10px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        height: 2rem;
        font-size: 0.8rem;
    }
`;

export default UpdateMenu

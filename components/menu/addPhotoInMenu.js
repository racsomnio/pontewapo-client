import { useState } from 'react'
import styled from 'styled-components'
import StoredPics from '../common/StoredPics'

const AddPhotoInMenu = ({addPhotoToParentState, i, j, hasImage, storeId}) => {
    const [image, setImage] = useState("");
    const [ open, toggle ] = useState(false)

    function getImage (img) {
        addPhotoToParentState(img, i, j)
    }

    function openModal() {
        toggle(true);
    }

    function closeModal() {
        toggle(false);
    }

    return (
        <>
            { 
                hasImage
                    ? <div><img 
                            src={ hasImage } 
                            alt="menu"
                            style={{ borderRadius: '10px', marginLeft: '0.5rem', width: '90%' }}
                            onClick={openModal}
                        />
                        </div>
                    : <Button 
                            className="button"
                            onClick={openModal}
                        >Foto</Button>
            }
            <StoredPics isOpen={open} close={closeModal} getImage={getImage} storeId={storeId} />
        </>
    )
}

const Button = styled.span`
    /* background: #e6e7e8;
    color: #7a7a7a; */
    font-weight: bold;
    padding: 0.5rem 1rem;
    font-size: 1rem;
    font-family: Arial, Helvetica, sans-serif;
    display: block;
    border-radius: 10px;
    align-self: flex-start;
    font-weight: normal;
`;

export default AddPhotoInMenu;

import styled from 'styled-components';
import { useMutation } from '@apollo/react-hooks'
import { UPLOAD_IMAGE } from '../../queries';
import Loading from '../common/Loading';

const AddPhoto = ({addPhotoToParentState, hasImage}) => {

    const convertTo64base = (e, callback) => {
        e.persist();

        const FR= new FileReader();
        FR.addEventListener("load", async function(event) {
            // console.log( event.target.result );
            const res = await callback({
                variables: { img64base: event.target.result }
            });
            // console.log(res);
            const { __typename, ...toState } = res.data.uploadImage;
            addPhotoToParentState(toState);

        }); 
          
        FR.readAsDataURL( e.target.files[0] );
    }

    const [uploadImage, {loading}] = useMutation(UPLOAD_IMAGE);

    return (
        <Label>
            { loading 
                ? <Loading invert={true} /> 
                : (
                    hasImage.length
                    ? <img 
                        src={ hasImage[0].secure_url } 
                        alt="menu"
                        style={{ width: '200px', borderRadius: '10px' }}
                    />
                    : <span className="button">Agregar Foto de Comida</span>
                )
            }
            <input 
                className="input-file" 
                type='file' 
                accept='image/*'
                onChange={(e) => convertTo64base(e, uploadImage)}
            />
        </Label>
    )
}

const Label = styled.label`
    cursor: pointer;

    .button {
        background: #7b057b;
        color: #fff;
        font-weight: bold;
        padding: 0.5rem 1rem;
        font-size: 1rem;
        font-family: 'Baloo 2',cursive;
        margin-top: 0.5rem;
        border-radius: 3rem;
    }
    
    .input-file {
        display: none;
    }
`;

export default AddPhoto;

import styled from 'styled-components';

const DayItem = ({ day, selected }) => {
    const [ isSelected, setIsSelected ] = React.useState(selected || false);
    
    const toggleClass = () => {
        const currentState = !isSelected;
        setIsSelected(currentState);
    }

    return (
        <Day 
            className={`day ${isSelected ? 'selected': ''}`} 
            onClick={toggleClass} 
        >
            { day }
        </Day>
    )
}

const Day = styled.span`
    padding: 0 0.5rem;
    cursor: pointer;
    font-weight: bold;
    color: #bbb;
    border-radius: 10px;
    margin: 0.2rem;
    padding: 0.2rem 0.4rem;
    transition: background, color 0.5s;

    &.selected {
        background: #7a1a7b;
        color: #fff;
        
    }
`;

export default DayItem

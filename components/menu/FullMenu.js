import { useQuery } from "@apollo/react-hooks"
import { GET_MENU } from '../../queries'
import Head from 'next/head'
import dynamic from 'next/dynamic'
import styled from 'styled-components'
import Loading from '../common/Loading'
import Error from '../common/Error'
import { Icon, Divider } from '../../styles/common';
import ShowLocation from '../common/ShowLocation'
import { daysLong, countryToFlag } from '../../utils';

const ShareButtons = dynamic(
    () => import('./ShareButtons'),
    { ssr: false }
)

const EditMenuNav = dynamic(
    () => import('./EditMenuNav'),
    { ssr: false }
)

function FullMenu({ meal, menuId }) {
    const { loading, error, data } = useQuery(GET_MENU, {
        variables: { _id: menuId },
    });

    const today = daysLong[new Date().getDay()];

    if(loading) return <Loading />
    if(error) return <Error error={error}/>

    const hasImage = data.getMenu.menu_image.length 
        ? data.getMenu.menu_image[0].secure_url
        : ''; 

    console.log(data);

    return (
        <div>
            <Head>
                <title>{data.getMenu.owner.name}</title>
                <meta property="og:title" content={data.getMenu.owner.name} />
                <meta property="og:image" content={hasImage} />
                <meta property="og:type" content="website" />
                <meta name="description" property="og:description" content={`${meal} - Te invitamos a ver nuestro menú para hoy ${today}. Esperamos verte por aquí, puedes contactarnos directamente a través de este enlace.`} />
                <meta property="og:locale" content="es_MX" />
                <meta property='twitter:title' content={data.getMenu.owner.name} />
                <meta property='twitter:image' content={hasImage}/>
                <meta name="twitter:card" content="summary_large_image"/>
                
            </Head>

            <EditMenuNav menuId={data.getMenu._id} ownerId={data.getMenu.owner._id} />

            <FullMenuGrid>
                <Col>
                    <StoreInfo>
                        {
                            data.getMenu.owner.profile_img.length ?
                            <img 
                                src={data.getMenu.owner.profile_img[0].secure_url} 
                                style={{
                                    borderRadius: '10px',
                                    margin: 'auto',
                                    width: '100px',
                                }}
                            />
                            : null
                        }
                        
                        <h2 style={{ margin: '1rem auto 0.3rem'}}>
                            {data.getMenu.owner.name}
                        </h2>
                        <div style={{ marginBottom: '1rem', textTransform: 'capitalize' }}>
                            <small>{data.getMenu.owner.typeOfBusiness}</small>
                        </div>
        
                        <div style={{ textAlign: 'left', padding: '1rem' }}>
                            <div style={{ marginBottom: '1.5rem', fontSize: '0.8rem' }}>
                                <ShowLocation location={data.getMenu.location}>
                                    {data.getMenu.location.address}
                                </ShowLocation>
                            </div>
                            {   data.getMenu.owner.food_type.type &&
                                <div style={{ textAlign: 'left', marginBottom: '1rem' }}>
                                    <span style={{ fontSize: '1.2rem', verticalAlign: 'middle'}}>{countryToFlag(data.getMenu.owner.food_type.code)}</span>
                                    <small>&nbsp;Comida {data.getMenu.owner.food_type.type}</small>
                                </div>
                            }
                            <Phone style={{ marginBottom: '1rem' }}>
                                <Icon size={'20px'}><img src="/icons/phone.svg" alt="tel" /></Icon>
                                <a href={`tel:+52${data.getMenu.owner.phone[0].number}`}>&nbsp;Llama al <strong>{data.getMenu.owner.phone[0].number}</strong></a>
  
                            </Phone>
                            <div style={{ textAlign: 'left', marginBottom: '1.5rem', borderBottom: '1px solid #e6e7e8', paddingBottom: '0.5rem' }}>
                                {
                                    data.getMenu.owner.phone[0].whatsapp && (
                                        <>
                                            <Icon size={'20px'}><img src="/icons/whatsapp.svg" alt="whatsapp" /></Icon>
                                            <a 
                                                href={`https://wa.me/52${data.getMenu.owner.phone[0].number}?text=Hola%2C%20v%C3%AD%20tu%20men%C3%BA%20en%20Comida%20God%C3%ADn%2C%20`}
                                            >&nbsp;Envía un <strong>Whatsapp</strong></a>
                                        </>
                                    )
                                }
                            </div>
                        </div>

                        <Options>
                            {
                                data.getMenu.owner.delivery &&
                                <span>
                                    <Icon size="30px"><img src="/icons/delivery.svg"/></Icon>
                                    <span>Entrega a domicilio</span>
                                </span>
                            }
                            {
                                data.getMenu.owner.takeaway &&
                                <span>
                                    <Icon size="30px"><img src="/icons/takeaway.svg"/></Icon>
                                    <span>Para llevar</span>
                                </span>
                                
                            }
                      
                            {
                                data.getMenu.owner.only_cash &&
                                <span>
                                    <Icon size="30px"><img src="/icons/cash.svg"/></Icon>
                                    <span>Sólo Efectivo</span>
                                </span>
                                
                            }
                        
                            {
                                data.getMenu.owner.card &&
                                <span>
                                    <Icon size="30px"><img src="/icons/card.svg"/></Icon>
                                    <span>Se aceptan tarjetas</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.reservations &&
                                <span>
                                    <Icon size="30px"><img src="/icons/reservations.svg"/></Icon>
                                    <span>Reservaciones</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_breakfast &&
                                <span>
                                    <Icon size="30px"><img src="/icons/breakfast.svg"/></Icon>
                                    <span>Desayunos</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_lunch_special &&
                                <span>
                                    <Icon size="30px"><img src="/icons/lunch-special.svg"/></Icon>
                                    <span>Menú del día</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.parking &&
                                <span>
                                    <Icon size="30px"><img src="/icons/parking.svg"/></Icon>
                                    <span>Estacionamiento</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.valet_parking &&
                                <span>
                                    <Icon size="30px"><img src="/icons/valet-parking.svg"/></Icon>
                                    <span>Valet parking</span>
                                </span>
                                
                            }  
                            {
                                data.getMenu.owner.wifi &&
                                <span>
                                    <Icon size="30px"><img src="/icons/wifi.svg"/></Icon>
                                    <span>Wifi</span>
                                </span>
                                
                            }                          
                            {
                                data.getMenu.owner.open_tv &&
                                <span>
                                    <Icon size="30px"><img src="/icons/tv.svg"/></Icon>
                                    <span>TV abierta</span>
                                </span>
                                
                            }
                        
                            {
                                data.getMenu.owner.cable &&
                                <span>
                                    <Icon size="30px"><img src="/icons/tv.svg"/></Icon>
                                    <span>Cable</span>
                                </span>
                                
                            }
                        
                            {
                                data.getMenu.owner.ppv &&
                                <span>
                                    <Icon size="30px"><img src="/icons/ppv.svg"/></Icon>
                                    <span>Pago Por Evento</span>
                                </span>
                                
                            }
                       
                            {
                                data.getMenu.owner.speak_english &&
                                <span>
                                    <Icon size="30px"><img src="/icons/speaking.svg"/></Icon>
                                    <span>We Speak English</span>
                                </span>
                                
                            }
                        
                            {
                                data.getMenu.owner.english_menu &&
                                <span>
                                    <Icon size="30px"><img src="/icons/menu.svg"/></Icon>
                                    <span>English Menu</span>
                                </span>
                                
                            }
                      
                            {
                                data.getMenu.owner.vegan_option &&
                                <span>
                                    <Icon size="30px"><img src="/icons/vegan.svg"/></Icon>
                                    <span>Opción Vegana</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.vegetarian_option &&
                                <span>
                                    <Icon size="30px"><img src="/icons/vegetarian.svg"/></Icon>
                                    <span>Opción Vegetariana</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.no_gluten_option &&
                                <span>
                                    <Icon size="30px"><img src="/icons/gluten-free.svg"/></Icon>
                                    <span>Sin Gluten</span>
                                </span>
                                
                            }
                        
                            {
                                data.getMenu.owner.no_shellfish_option &&
                                <span>
                                    <Icon size="30px"><img src="/icons/shrimp.svg"/></Icon>
                                    <span>Opción sin mariscos</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.live_music &&
                                <span>
                                    <Icon size="30px"><img src="/icons/live-music.svg"/></Icon>
                                    <span>Música en vivo</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.dancing &&
                                <span>
                                    <Icon size="30px"><img src="/icons/dancing.svg"/></Icon>
                                    <span>Se puede bailar</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_karaoke &&
                                <span>
                                    <Icon size="30px"><img src="/icons/karaoke.svg"/></Icon>
                                    <span>Karaoke</span>
                                </span>
                                
                            }                            
                            {
                                data.getMenu.owner.rooftop &&
                                <span>
                                    <Icon size="30px"><img src="/icons/rooftop.svg"/></Icon>
                                    <span>Rooftop ó Skybar</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.terrace &&
                                <span>
                                    <Icon size="30px"><img src="/icons/terrace.svg"/></Icon>
                                    <span>Terraza</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.garden &&
                                <span>
                                    <Icon size="30px"><img src="/icons/garden.svg"/></Icon>
                                    <span>Jardín</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.smoking_area &&
                                <span>
                                    <Icon size="30px"><img src="/icons/smoking-area.svg"/></Icon>
                                    <span>Área para fumar</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.kids_menu &&
                                <span>
                                    <Icon size="30px"><img src="/icons/kids-menu.svg"/></Icon>
                                    <span>Menú para niños</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.kids_area &&
                                <span>
                                    <Icon size="30px"><img src="/icons/kids-area.svg"/></Icon>
                                    <span>Área para niños</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.private_room &&
                                <span>
                                    <Icon size="30px"><img src="/icons/private-room.svg"/></Icon>
                                    <span>Área privada</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.pet_friendly &&
                                <span>
                                    <Icon size="30px"><img src="/icons/pet-friendly.svg"/></Icon>
                                    <span>Pet friendly</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.catering &&
                                <span>
                                    <Icon size="30px"><img src="/icons/catering.svg"/></Icon>
                                    <span>Catering</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.gay &&
                                <span>
                                    <Icon size="30px"><img src="/icons/gay.svg"/></Icon>
                                    <span>Gay</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_healthy_food &&
                                <span>
                                    <Icon size="30px"><img src="/icons/healthy-food.svg"/></Icon>
                                    <span>Saludable</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_alcohol &&
                                <span>
                                    <Icon size="30px"><img src="/icons/alcohol.svg"/></Icon>
                                    <span>Bebidas alcohólicas</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_billiard &&
                                <span>
                                    <Icon size="30px"><img src="/icons/billiard.svg"/></Icon>
                                    <span>Billar</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_foosball &&
                                <span>
                                    <Icon size="30px"><img src="/icons/foosball.svg"/></Icon>
                                    <span>Futbolito</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.has_pool &&
                                <span>
                                    <Icon size="30px"><img src="/icons/pool.svg"/></Icon>
                                    <span>Piscina</span>
                                </span>
                                
                            }
                            {
                                data.getMenu.owner.big_groups &&
                                <span>
                                    <Icon size="30px"><img src="/icons/big-groups.svg"/></Icon>
                                    <span>Maneja grupos grandes</span>
                                </span>
                                
                            }
                        </Options>
                        
                        <div style={{ borderTop: '1px solid #e6e7e8', paddingTop: '2rem', marginTop: '2rem'}}>
                            {
                                data.getMenu.owner.hours.map( hour => <small>
                                        <strong>{`${hour.from} a ${hour.to}`}</strong>
                                        <span>{` de ${hour.open_hr}:${hour.open_min} ${hour.open_ampm} a ${hour.close_hr}:${hour.close_min} ${hour.close_ampm}`}</span>
                                    </small>
                                )
                            }
                        </div>
                    </StoreInfo>
                </Col>
                
                <Col>
                    <MenuInfo>
                        <span className="info-title">Menú</span>
                        <div 
                            dangerouslySetInnerHTML={{__html: data.getMenu.html_menu}}
                        />
                        <Image>
                            {
                                data.getMenu.menu_image.length &&
                                <img 
                                    src={data.getMenu.menu_image[0].secure_url} 
                                    alt="menu" 
                                />
                            }
                        </Image>
                        {
                            data.getMenu.owner.speciality.length 
                            ? <>
                                <Icon size="20px"><img src="/icons/recommended.svg"/></Icon>
                                <small> Platillo más popular: </small>
                                <strong>{data.getMenu.owner.speciality[0]}</strong>
                            </>
                            : null
                        }
                        

                    </MenuInfo>
                
                    <ShareButtons 
                        url={`/${meal}/${menuId}`} 
                        text={`${meal.toUpperCase()} - Te invitamos a ver nuestro menú para hoy ${today}. Esperamos verte por aquí, puedes contactarnos directamente a través de este enlace.`} 
                        media={hasImage}
                    />
                </Col>

                
            </FullMenuGrid>
        </div>
    )
}

const FullMenuGrid = styled.div`
    display: flex;
    justify-content: left;
    flex-wrap: wrap;
    justify-content: center;
`;

const Phone = styled.div`
    margin-bottom: 1rem;
    background: #fafafa;
    margin-left: -3rem;
    margin-right: -3rem;
    padding: 0.6rem 1rem 0.6rem 3rem;
    border-radius:10px 10px 2px 2px;
    text-align: left;
    position: relative;
    border: 1px solid #e6e7e8;

    &:before, &:after {
        content: "";
        position: absolute;
        z-index: -1;
        width: 1rem;
        height: 1rem;
        background: #e6e7e8;
        border-radius: 0 0 0 0.5rem;
        left: 0;
        bottom: -0.4rem;
    }
    &:after {
        left: initial;
        right: 0;
        border-radius: 0 0 0.5rem 0;
    }
`;

const Image = styled.div`
    padding-right: 0;
    margin-bottom: 1rem;
    margin-top: 1.5rem;

    & img {
        border-radius: 10px;
    }
`;

const Col = styled.div`
    flex: 0 1 100%;
    text-align: left;
    padding: 0 1rem;

    @media all and (min-width: 600px){
        flex: 0 1 400px;
    }
`;

const StoreInfo = styled.div`
    text-align: center;
    position: relative;
    padding: 1.5rem 1.5rem 2rem;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    background: #fff;
    margin-bottom: 2.5rem;
`;

const Options = styled.div`
    font-size: 0.75rem;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;

    & > span {
        padding: 1rem;
        flex: 1 1 50%;

        & > span {
            display: block;
            margin: auto;

            &:last-child {
                background: #fafafa;
                border: 1px solid #e6e7e8;
                border-radius: 20px;
                padding: 0.4rem;
                margin-top: 0.3rem;
            }
        }
    }
`;

const MenuInfo = styled.div`
    position: relative;
    padding: 2.5rem 1.5rem 2rem;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    background: #fff;
    margin-bottom: 1.5rem;

    @media all and (min-width: 600px) {
        margin-top: 4rem;
    }

    h2 {
        text-transform: capitalize;
        margin-top: 0;
        margin-bottom: 0;
        line-height: 1;
        font-size: 1.25rem;
        text-align: center !important;
    }

    p + h2 {
        margin-top: 2rem;
    }

    p {
        margin: 0;
        text-align: center !important;
    }

    .info-title {
        position: absolute;
        left:  50%;
        transform: translateX(-50%);
        border-radius: 1rem;
        font-weight: bold;
        /* border-left: 4px solid #e5e5e5; */
        /* border-right: 4px solid #e5e5e5; */
        /* background: rgb(122, 26, 123); */
        background: #fafafa;
        color: #000;
        padding: 0.3rem 2rem;
        top: -1.15rem;
        border: 1px solid #e6e7e8;
        font-family: 'Baloo 2',cursive;
    }
`;
export default FullMenu

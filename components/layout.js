import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Header from './header'
import Footer from './footer'

const useStyles = makeStyles({
  container: {
    minHeight: '100vh',
    flexDirection: 'column'
  },

  main: {
    textAlign: 'center',
    paddingBottom: '5rem'
  }
});

function Layout({ children, location }) {
  const classes = useStyles();
  const isDigitaMenu = location === '/[meal]';

  return (
    <Grid container justify="space-between" className={classes.container}>
        <Header/>
      
        <main className={classes.main}>
          {children}
        </main>
      
        <Footer/>
    </Grid>
  )
}
export default Layout
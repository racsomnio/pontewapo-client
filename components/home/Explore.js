import Link from 'next/link'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import MyLocationIcon from '@material-ui/icons/MyLocation';
import Box from '@material-ui/core/Box'

const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(6, 2, 6),
    },
    button: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(5),
    }
}));

function Explore() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid
                    item 
                    xs
                >
                    <Typography align="center" variant="h4" component="h2" align="left" pl={2}>
                        <Box pl={4}>
                            <Typography color="primary" variant="h4" component="span">Ponte WApo</Typography> y mira lo que hay alrededor
                        </Box>
                    </Typography>
                </Grid>
                <Grid
                    item
                    xs
                >
                    <img src="/home/find-location.svg" alt="Ponte Wapo alrededor" />
                </Grid>

                <Grid 
                    item
                    xs={12}
                >
                    <Button
                        variant="contained"
                        color="primary"
                        startIcon={<MyLocationIcon/>}
                        className={ classes.button }
                    >
                        Activa Mi Ubicación
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default Explore

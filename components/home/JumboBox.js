import Link from 'next/link'
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import ExploreIcon from '@material-ui/icons/Explore';
import StorefrontIcon from '@material-ui/icons/Storefront';

const useStyles = makeStyles((theme) => ({
    root: {
        background: theme.palette.primary.main,
        // marginTop: '-75px',
        padding: theme.spacing(6, 2, 8),
        flexGrow: 1,
    },
    grid: {
        [theme.breakpoints.up('sm')]: {
            height: '360px',
        },
    },
    img: {
        width: '80%',
        maxHeight: '300px',
        margin: theme.spacing(5, 0)
    },

    button: {
        marginTop: theme.spacing(2),
    }

}));

function JumboBox() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid 
                container 
                direction="column"
                wrap="wrap"
                className={classes.grid}
            >
                <Box clone order={{ xs: 1, sm: 2 }}>
                    <Grid 
                        item container
                        sm={6}
                        alignItems="flex-end"
                        justify="center"
                    >
                        <Typography variant="h4" color="secondary" component="h1">
                            Agilizamos tus pedidos por WhatsApp
                        </Typography>
                    </Grid>
                </Box>

                <Box clone order={{ xs: 2, sm: 1 }}>
                    <Grid 
                        item 
                        container 
                        justify="center"
                        sm={6}
                    >
                        <img className={classes.img} src="/home/cta-image.svg" alt="recibe pedidos por WhatsApp" />
                    </Grid>
                </Box>

                <Box clone order={{ xs: 3, sm: 3 }}>
                    <Grid item 
                        justify="space-evenly" 
                        container
                        sm={6}
                        alignItems="flex-start"
                    >
                        <Link href="/negocios/registro" passHref>
                            <Button
                                component="a" 
                                startIcon={<StorefrontIcon />}
                                color="secondary"
                                variant="outlined"
                                className={classes.button}
                            >
                                Registra Tu Negocio
                            </Button>
                        </Link>
                        <Link href="/negocios/registro" passHref>
                            <Button
                                component="a" 
                                startIcon={<ExploreIcon />}
                                color="secondary"
                                variant="contained"
                                className={classes.button}
                            >
                                Descubre
                            </Button>
                        </Link>
                    </Grid>
                </Box>
            </Grid>
        </div>
    )
}

export default JumboBox

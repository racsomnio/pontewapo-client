import Link from 'next/link'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import MyLocationIcon from '@material-ui/icons/MyLocation';
import Divider from '@material-ui/core/Divider'
import Box from '@material-ui/core/Box'


const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(0, 2),
    },
    button: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(5),
    }
}));

function Pedidos() {
    const classes= useStyles();

    return (
        <div className={classes.root}>
            <Grid
                container
            >
                <Grid
                    item 
                    xs
                >
                    <Divider />
                    <Typography align="center" variant="overline" >
                        Deja atrás los grupos. 
                    </Typography>
                    <Divider />
                    <Typography
                        variant="h2"
                        component="h4"
                    >
                        <Box py={8}>
                            <Typography color="primary" variant="span">Ponte WApo,</Typography> y sácale provecho
                            al WhatsApp
                        </Box>
                    </Typography>
                </Grid>
                <Grid
                    item 
                    xs={12}
                >
                    <img src="/home/texting-chair.svg" alt="pedidos por whatsapp"/>
                </Grid>

                <Box pt={4}>
                    <Divider />
                    <Typography variant="overline">
                        Crea tu catalogo en línea y comienza a recibir
                        órdenes por WhatsApp
                    </Typography>
                    <Divider />
                </Box>
            </Grid>
        </div>
    )
}

export default Pedidos

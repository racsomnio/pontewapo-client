import OptionCheckbox from './OptionCheckbox';
import styled from 'styled-components'

const UpdateOptions = ({ data }) => {

    return (
        <div>
            <h4 style={{ marginBottom: '2rem' }}>Activa el que se ajuste a tu establecimiento</h4>

            <GridContainer>
                <OptionCheckbox
                    flag="wa_orders" 
                    res={data.wa_orders} 
                    icon="whatsapp.svg"
                    title="Aceptamos Órdenes por WhatsApp" 
                />
                <OptionCheckbox
                    flag="delivery" 
                    res={data.delivery} 
                    icon="delivery.svg"
                    title="Entrega a domicilio" 
                />

                <OptionCheckbox 
                    flag="takeaway" 
                    res={data.takeaway} 
                    icon="takeaway.svg"
                    title="Para llevar" 
                />

                <OptionCheckbox 
                    flag="only_cash" 
                    res={data.only_cash} 
                    icon="cash.svg"
                    title="Sólo Efectivo" 
                />  

                <OptionCheckbox
                    flag="card" 
                    res={data.card} 
                    icon="card.svg"
                    title="Aceptamos Tarjetas" 
                />

                <OptionCheckbox
                    flag="has_breakfast" 
                    res={data.has_breakfast} 
                    icon="breakfast.svg"
                    title="Desayunos" 
                /> 

                <OptionCheckbox
                    flag="has_lunch_special" 
                    res={data.has_lunch_special} 
                    icon="lunch-special.svg"
                    title="Menú del día" 
                />  

                <OptionCheckbox
                    flag="parking" 
                    res={data.parking} 
                    icon="parking.svg"
                    title="Estacionamiento" 
                />  

                <OptionCheckbox
                    flag="valet_parking" 
                    res={data.valet_parking} 
                    icon="valet-parking.svg"
                    title="Valet parking" 
                />                  

                <OptionCheckbox
                    flag="open_tv" 
                    res={data.open_tv} 
                    icon="tv.svg"
                    title="TV abierta" 
                />  

                <OptionCheckbox 
                    flag="cable" 
                    res={data.cable} 
                    icon="tv.svg"
                    title="Cable" 
                />  

                <OptionCheckbox
                    flag="ppv" 
                    res={data.ppv} 
                    icon="ppv.svg"
                    title="PPV" 
                />  

                <OptionCheckbox
                    flag="wifi" 
                    res={data.wifi} 
                    icon="wifi.svg"
                    title="Wifi" 
                />  

                <OptionCheckbox 
                    flag="speak_english" 
                    res={data.speak_english} 
                    icon="speaking.svg"
                    title="We speak English" 
                />  

                <OptionCheckbox
                    flag="english_menu" 
                    res={data.english_menu} 
                    icon="menu.svg"
                    title="English Menu" 
                />  

                <OptionCheckbox
                    flag="reservations" 
                    res={data.reservations} 
                    icon="reservations.svg"
                    title="Reservaciones" 
                />  

                <OptionCheckbox
                    flag="vegan_option" 
                    res={data.vegan_option} 
                    icon="vegan.svg"
                    title="Opción Vegana" 
                />  

                <OptionCheckbox
                    flag="vegetarian_option" 
                    res={data.vegetarian_option} 
                    icon="vegetarian.svg"
                    title="Opción Vegetariana" 
                />  

                <OptionCheckbox
                    flag="no_gluten_option" 
                    res={data.no_gluten_option} 
                    icon="gluten-free.svg"
                    title="Opción Sin Gluten" 
                />  

                <OptionCheckbox
                    flag="no_shellfish_option" 
                    res={data.no_shellfish_option} 
                    icon="shrimp.svg"
                    title="Opción sin Mariscos" 
                />            

                <OptionCheckbox
                    flag="has_healthy_food" 
                    res={data.has_healthy_food} 
                    icon="healthy-food.svg"
                    title="Comida Saludable" 
                />

                <OptionCheckbox
                    flag="live_music" 
                    res={data.live_music} 
                    icon="live-music.svg"
                    title="Música en Vivo" 
                /> 

                <OptionCheckbox
                    flag="dancing" 
                    res={data.dancing} 
                    icon="dancing.svg"
                    title="Se puede bailar" 
                />  

                <OptionCheckbox
                    flag="has_karaoke" 
                    res={data.has_karaoke} 
                    icon="karaoke.svg"
                    title="karaoke" 
                />

                <OptionCheckbox
                    flag="rooftop" 
                    res={data.rooftop} 
                    icon="rooftop.svg"
                    title="Rooftop ó Skybar" 
                /> 

                <OptionCheckbox
                    flag="terrace" 
                    res={data.terrace} 
                    icon="terrace.svg"
                    title="Terraza" 
                /> 

                <OptionCheckbox
                    flag="garden" 
                    res={data.garden} 
                    icon="garden.svg"
                    title="Jardín" 
                />  

                <OptionCheckbox
                    flag="smoking_area" 
                    res={data.smoking_area} 
                    icon="smoking-area.svg"
                    title="Área para fumar" 
                />  
                
                <OptionCheckbox
                    flag="private_room" 
                    res={data.private_room} 
                    icon="private-room.svg"
                    title="Área privada" 
                />                 

                <OptionCheckbox
                    flag="kids_menu" 
                    res={data.kids_menu} 
                    icon="kids-menu.svg"
                    title="Menú para niños" 
                />  

                <OptionCheckbox
                    flag="kids_area" 
                    res={data.kids_area} 
                    icon="kids-area.svg"
                    title="Área para niños" 
                />   

                <OptionCheckbox
                    flag="pet_friendly" 
                    res={data.pet_friendly} 
                    icon="pet-friendly.svg"
                    title="Pet friendly" 
                /> 

                <OptionCheckbox
                    flag="catering" 
                    res={data.catering} 
                    icon="catering.svg"
                    title="Catering" 
                />  

                <OptionCheckbox
                    flag="gay" 
                    res={data.gay} 
                    icon="gay.svg"
                    title="LGBTQ" 
                /> 

                <OptionCheckbox
                    flag="has_alcohol" 
                    res={data.has_alcohol} 
                    icon="alcohol.svg"
                    title="Bebidas alcohólicas" 
                />

                <OptionCheckbox
                    flag="has_billiard" 
                    res={data.has_billiard} 
                    icon="billiard.svg"
                    title="Billar" 
                />

                <OptionCheckbox
                    flag="has_foosball" 
                    res={data.has_foosball} 
                    icon="foosball.svg"
                    title="Futbolito" 
                />

                <OptionCheckbox
                    flag="has_pool" 
                    res={data.has_pool} 
                    icon="pool.svg"
                    title="Piscina" 
                />

                <OptionCheckbox
                    flag="big_groups" 
                    res={data.big_groups} 
                    icon="big-groups.svg"
                    title="Grupos grandes" 
                />
                
            </GridContainer>
        </div>
    )
}

const GridContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(254px, 1fr));
    max-width: 800px;
    margin: auto;
    height: 210px;
    justify-content: flex-start;
    overflow: scroll;
`;

export default UpdateOptions;


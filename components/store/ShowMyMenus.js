import Link from 'next/link'
import { useQuery } from '@apollo/react-hooks'
import styled from 'styled-components';
import { GET_MY_MENUS } from '../../queries';
import Error from '../common/Error';
import Loading from '../common/Loading';
import { swap_it, days } from '../../utils';

function ShowMyMenus() {
    const getMealKey = (meal) => {
        const keys = Object.keys(swap_it).filter((k) => {
            if(swap_it[k] == meal) return k
        });
        return keys[0];
    }
    
    const { loading, error, data } = useQuery(GET_MY_MENUS);

    if(loading) return <Loading/>
    if(error) return <Error error={error}/>

    return (
        <GridContainer>
            {
                data.getMyMenus.map(menu =>{
                    const kMeal = getMealKey(menu.meal);
                    console.log(kMeal.slice(0,-1));
                    
                    const daysNum = menu.repeat
                        .sort((a,b) => (a==0 || b==0) ? b - a : a -b) //This will put cero at the end instead of the begining
                        .map(v => days[v]) // this will change the number to day text
                        .join(" • ");
                    return (
                        <div key={menu._id}>
                            <Link href="/[meal]/[menuId]" as={`/${kMeal}/${menu._id}`}>
                                <a>
                                    <img src={menu.menu_image.length ? menu.menu_image[0].secure_url: '/no-image.png'} />
                                    <h1>{kMeal.slice(0,-1)}</h1>
                                    <div>{daysNum}</div>
                                </a>
                            </Link>
                        </div>
                    )
                })
            }
        </GridContainer>
    )
}
export default ShowMyMenus


const GridContainer = styled.div`
    margin: 1.5rem auto 0;
    display: grid;
    max-width: 1000px;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    column-gap: 20px;

    @media all and (min-width: 420px) {
        border-radius: 10px;
        padding-left: 1rem;
        padding-right: 1rem;
    }


    img {
        @media all and (min-width: 420px) {
            border-radius: 10px;
        }
    }

    h1 {
        margin-top: 0;
        margin-bottom: 0;
        font-size: 1.2rem;
    }

    div {
        margin-bottom: 1rem;
        font-size: 0.8rem;
    }
`;

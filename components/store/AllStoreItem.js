import styled from 'styled-components';
import Link from 'next/link'
import { swap_it, days, getMealInSpanish } from '../../utils';

function AllStoreItem({ store }) {
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    const localeDate = new Date(Number(store.createdDate)).toLocaleDateString('es-MX', options)
    const Menus = () => store.menus.map( menu => {
        const meal = getMealInSpanish(menu.meal, swap_it)
        return <li key={menu._id} style={{ padding: '0', marginBottom: '1rem'}}>
            <Link href={`/${meal}/${menu._id}`}>
                <a>{meal}</a>
            </Link>
        </li>
    }) 

    return (
        <StoreCard>
            <div className="store-profile-img"
                style={{ backgroundImage: `url(${store.profile_img.length ? store.profile_img[0].secure_url : '' })` }}
            ></div>
            <div style={{ flex: 'calc(50% + 100px)' }}>
                <Link href={`/negocios/${store._id}`}>
                    <a className="store-card-title">
                        {store.name}
                    </a> 
                </Link>
                <div><small><strong>Cuenta Creada:</strong> { localeDate }</small></div>
                <div><small><strong>Dirección:</strong> { store.location.address }</small></div>
                <div><small><strong>Teléfono:</strong> { store.phone[0].number }</small></div>
            </div>
            <div>
                <ul>
                    <Menus/>
                </ul>
            </div>
            
        </StoreCard>
    )
}

const StoreCard = styled.div`
    max-width: 700px;
    margin: 1rem auto 3rem;
    display: flex;
    text-align: left;
    padding: 0 1rem;
    flex-wrap: wrap;
    
    .store-profile-img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        border: 1px solid rgba(0,0,0,0.3);
        background-size: cover;
        margin-right: 1rem;
        flex: 0 0 100px;
    }

    .store-card-title {
        font-size: 1.5rem;
        display: block;
    }
`;

export default AllStoreItem

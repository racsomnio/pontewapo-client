import styled from 'styled-components';
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_STORE_IMGS, GET_MY_STORE } from '../../queries';
import { ImgContainer } from '../../styles/common';
import Loading from '../common/Loading';
import { convertTo64base } from '../../utils'

const UploadCover = ({ img, field }) => {
    const [updateStoreImgs, {loading}] = useMutation(UPDATE_STORE_IMGS,{
        refetchQueries: [{query: GET_MY_STORE}],
        awaitRefetchQueries: true,
    })

    return (
        // <ImgContainer>
            <UploadImg>
                <label className={`${field} ${ !img[0] && 'no-image' }`} 
                    style={ 
                        img.length 
                        ? {backgroundImage: `url(${img[0].secure_url})`}
                        : {backgroundImage: `url(/icons/add-image.svg)`}
                    } 
                >
                    { loading && <Loading /> }

                    <input 
                        className="input-file" 
                        type='file' 
                        accept='image/*'
                        onChange={(e) => convertTo64base(e, field, updateStoreImgs)}
                    />
                </label>
            </UploadImg>
        // </ImgContainer>
    )
}

const UploadImg = styled.div`
    height: inherit;
    width: inherit;

    label {
        &.cover_img {
            position: absolute;
            background-size: auto;
            background-repeat: no-repeat;
            background-position: bottom center;
            background-attachment: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;

            @media all and (min-width: 500px) {
                background-size: contain;
            }

            @media all and (min-width: 900px) {
                background-position: calc(50% + 37px);
            }

            @supports (-webkit-overflow-scrolling: touch) {
                background-attachment: scroll;
            }
        }

        &.profile_img {
            width: 150px;
            height: 150px;
            border: 5px solid #f2f2f2;
            border-radius: 50%;
            margin: auto;
            background: #bcbec0;
            position: relative;
            overflow: hidden;
            display: flex;
            justify-content: center;
            align-items: center;
            background-position: center;
            background-size: cover;
            cursor: pointer;

            &.no-image {
                background-size: 50px;
            }
        }

        &.no-image{
            background-color: #bcbec0;
            background-size: 15%;
            background-position: center;
            filter: invert(0.87);
        }
    }

    .input-file {
        display: none;
    }

`;

export default UploadCover;



import Link from 'next/link'
import { useQuery } from '@apollo/react-hooks'
import { GET_MY_STORE } from '../../queries'
import { GridCols, Steps } from '../../styles/common'
import UpdateProfile from './UpdateProfile'
import Loading from '../common/Loading'
import Error from '../common/Error'



function ShowProfile() {
    const { loading, error, data } = useQuery(GET_MY_STORE);

    if(loading) return <Loading />
    if(error) return <Error error={error} />
    
    return (
        <>
            <UpdateProfile data={data.getMyStore}/>
            
            <GridCols>
                <div className="col">
                    <Link href="/negocios/subir-menu">
                        <a className="button secondary">Siguiente</a>
                    </Link>
                </div>
            </GridCols>

            <Steps>2/3</Steps>  
        </>
    )
}

export default ShowProfile

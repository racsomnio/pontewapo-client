import { useState, useCallback } from 'react'
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_STORE_SPECIALITY, GET_MY_STORE } from '../../queries';
import { debounce } from 'lodash'

const UpdateFoodType = ({ speciality }) => {
    const [ dish, setDish ] = useState(speciality[0] || [])
    
    const [ updateStoreSpeciality, {loading} ] = useMutation(UPDATE_STORE_SPECIALITY, {
        variables: { speciality: dish },
        refetchQueries: [{query: GET_MY_STORE}],
    })

    const delayedMutation = useCallback(debounce(updateStoreSpeciality, 500), []);

    async function handleOnChange(e) {
        const { value } = e.target;
        setDish(value)
        delayedMutation(value)
    }

    return (
        <span>
            <input 
                type="text" 
                placeholder="Ej: Milanesa de pollo" 
                value={dish} 
                onChange={handleOnChange}
            />
        </span>
    )
}

export default UpdateFoodType

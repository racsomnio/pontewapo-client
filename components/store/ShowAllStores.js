import { useQuery } from '@apollo/react-hooks'
import { GET_ALL_STORES } from '../../queries';
import Error from '../common/Error';
import Loading from '../common/Loading';
import AllStoreItem from './AllStoreItem'

function ShowAllStores() {
    const { loading, error, data } = useQuery(GET_ALL_STORES);
    if(loading) return <Loading/>
    if(error) return <Error error={error}/>

    return (
        <div>
            {
                data.getAllStores.map( store => <AllStoreItem 
                    key={store._id}
                    store={store}>
                        {store.name}
                </AllStoreItem>)
            }
        </div>
    )
}

export default ShowAllStores

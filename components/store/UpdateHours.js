import { useState } from 'react'
import styled from 'styled-components'
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_STORE_HOURS, GET_MY_STORE } from '../../queries';

function UpdateHours({ storeHours }) {
    const blankHours = { from: "", to: "", open_hr:"", open_min:"", open_ampm:"", close_hr: "", close_min: "", close_ampm: ""};
    const [ hours, setHours ] = useState(storeHours.length ? storeHours : [{...blankHours}]);

    const [ updateStoreHours, {loading} ] = useMutation(UPDATE_STORE_HOURS, {
        refetchQueries: [{query: GET_MY_STORE}],
    })

    function addBlankHour() {
        setHours([...hours, {...blankHours}])
    }

    function handleChange(e, i) {
        e.persist();
        const { name, value } = e.target;
        const prevHours = hours;
        prevHours[i][name] = value;

        setHours([...prevHours]);
        updateStoreHours({ variables: { hours: prevHours} })
    }

    return (
        <Hours>
            <div style={{ marginBottom: '1rem'}}>
                <small style={{ marginBottom: '0.5rem' }} ><strong>Horario</strong></small>
            </div>

            {
                hours.map( (hour, i) => <div key={i} className="fields">
                        <div>
                            <div>
                                <small>De&nbsp;</small>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].from}
                                        name="from"
                                    >
                                        <option></option>
                                        <option value="Lunes">Lunes</option>
                                        <option value="Martes">Martes</option>
                                        <option value="Miércoles">Miércoles</option>
                                        <option value="Jueves">Jueves</option>
                                        <option value="Viernes">Viernes</option>
                                        <option value="Sábado">Sábado</option>
                                        <option value="Domingo">Domingo</option>
                                    </Select>
                                </span>
                            </div>
                            <div>
                                <small>&nbsp;A&nbsp;</small>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].to}
                                        name="to"
                                    >
                                        <option></option>
                                        <option value="Lunes">Lunes</option>
                                        <option value="Martes">Martes</option>
                                        <option value="Miércoles">Miércoles</option>
                                        <option value="Jueves">Jueves</option>
                                        <option value="Viernes">Viernes</option>
                                        <option value="Sábado">Sábado</option>
                                        <option value="Domingo" defaultValue>Domingo</option>
                                    </Select>
                                </span>
                            </div>
                        </div>

                        <div className="secondaryFields">
                            <div>
                                <small>Abre&nbsp;</small>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].open_hr}
                                        name="open_hr"
                                    >
                                        <option></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </Select>
                                </span>
                                <strong>:</strong>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].open_min}
                                        name="open_min"
                                    >
                                        <option></option>
                                        <option value="00">00</option>
                                        <option value="30">30</option>
                                    </Select>
                                </span>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].open_ampm}
                                        name="open_ampm"
                                    >
                                        <option></option>
                                        <option value="AM">AM</option>
                                        <option value="PM">PM</option>
                                    </Select>
                                </span>
                            </div>
                            <div>
                                <small>&nbsp;Cierra&nbsp;</small>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].close_hr}
                                        name="close_hr"
                                    >
                                        <option></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </Select>
                                </span>
                                <strong>:</strong>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].close_min}
                                        name="close_min"
                                    >
                                        <option></option>
                                        <option value="00">00</option>
                                        <option value="30">30</option>
                                    </Select>
                                </span>
                                <span className="select-wrap">
                                    <Select 
                                        onChange={(e) => handleChange(e, i)} 
                                        defaultValue={hours[i].close_ampm}
                                        name="close_ampm"
                                    >
                                        <option></option>
                                        <option value="AM">AM</option>
                                        <option value="PM">PM</option>
                                    </Select>
                                </span>
                            </div>
                        </div>
                </div>)
            }
            { hours.length <= 5 &&
                <SecondaryButton onClick={addBlankHour}>Agregar otro horario</SecondaryButton>
            }

        </Hours>
    )
}

const Hours = styled.div`
    div {
        margin-bottom: 0.5rem;
    }

    .fields {
        display: flex;
        flex-direction: column;
     & + .fields {
         margin-top: 2rem;
         padding-top: 3rem;
         border-top: 1px solid #e6e7e8;
     } 

        & div {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        & > div {
            flex-wrap: wrap;
        }
    }

    .secondaryFields {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;

        & > div {
            flex: 0 1 320px;
            align-items: center;
        }
    }

    .select-wrap {
        margin: initial;
    }
`;

const Select = styled.select`
    min-width: auto;
`

const SecondaryButton = styled.button`
    background: #fff;
    border: 1px solid #e6e7e8;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
    font-size: 0.8rem;
    color: #999;
`;


export default UpdateHours

import React from 'react'
import Autocomplete from '../common/Autocomplete'
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_STORE_FOOD_TYPE, GET_MY_STORE } from '../../queries';
import { countries } from '../../utils'

const UpdateFoodType = ({ food_type }) => {
    const [ updateStoreFoodType, {loading} ] = useMutation(UPDATE_STORE_FOOD_TYPE, {
        refetchQueries: [{query: GET_MY_STORE}],
    })
    
    return (
        <span>
            <Autocomplete 
                suggestions={countries} 
                placeholder="Ej: Mexicana, Italiana, ..." 
                defaultValue={food_type.type}
                callback={updateStoreFoodType}
            />
        </span>
    )
}

export default UpdateFoodType

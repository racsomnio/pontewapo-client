import { useState } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_STORE_PHONES, GET_MY_STORE } from '../../queries';
import { Icon, Checkbox } from '../../styles/common';

const EditPhones = ({ phone }) => {
    const [ phones, setPhones ] = useState(phone);
    const [phField, setPhField] = useState("");
    const [updateStorePhones, {loading}] = useMutation(UPDATE_STORE_PHONES, {
        refetchQueries: [{query: GET_MY_STORE}],
        awaitRefetchQueries: true,
    })

    const handleChange = async (callback, index) => {
        const p = [...phones];
        p[index].whatsapp = !p[index].whatsapp;
        await setPhones(p);
        await callback({
            variables: { phones: p },
        });
    }

    const addPhone = async () => {
        const newPhone = {number: phField, whatsapp: false};
        const p = [...phones, newPhone]
        setPhField("");
        setPhones(p)
        await updateStorePhones({
            variables: { phones: p }
        })
    }

    return (
        <>
            {
                phones.map( (ph, i) => (
                    <div key={ph.number}>
                        <Icon><img src='/icons/phone.svg' alt="mapa" /></Icon>
                        <span>{ph.number}</span>
                        <div>
                            <span>Tiene Whatsapp</span>
                            <Checkbox float="none" className="switch">
                                <input 
                                    type="checkbox" 
                                    checked={ph.whatsapp} 
                                    onChange={() => handleChange(updateStorePhones, i)} 
                                />
                                <span className="slider"></span>
                            </Checkbox>
                        </div>
                    </div>
                ))
            }
            {
                phones.length < 2 &&
                <div>
                    <input 
                        type="tel" 
                        maxLength="10"
                        value={phField} 
                        placeholder="Agrega otro número"
                        onChange={(e) => setPhField(e.target.value)}
                    />
                    <button 
                        onClick={() => addPhone()}
                        disabled={loading}
                    >
                        Agregar
                    </button>
                </div>
            }
        </>
    )
}

export default EditPhones

import PropTypes from 'prop-types'
import Link from 'next/link'
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx'
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import FingerprintIcon from '@material-ui/icons/Fingerprint';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import SettingsIcon from '@material-ui/icons/Settings';
import ExploreIcon from '@material-ui/icons/Explore';
import StorefrontIcon from '@material-ui/icons/Storefront';
import FacebookIcon from '@material-ui/icons/Facebook';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';

const useStyles = makeStyles({
    list: {
      width: 250,
    },
    fullList: {
      width: 'auto',
    },
});

const MenuItems = ({position, toggleDrawer}) => {
    const classes = useStyles();

    return (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: position === 'top' || position === 'bottom',
            })}
            role="presentation"
            onClick={toggleDrawer(position, false)}
            onKeyDown={toggleDrawer(position, false)}
        >
            <List>
                <ListItem>
                    <Link href="/negocios/registro" passHref>
                        <Button
                            component="a" 
                            startIcon={<StorefrontIcon />}
                            color="primary"
                            variant="outlined"
                        >
                            Registra Tu Negocio
                        </Button>
                    </Link>
                </ListItem>
            </List>
            
            <Link href="/ingresar" passHref>
                <ListItem
                    button 
                    component="a"
                >
                    <ListItemIcon>
                        <FingerprintIcon />
                    </ListItemIcon>
                    <ListItemText primary="Ingresar" />
                </ListItem>
            </Link>
            <Divider />
            
            <List>
                <Link href="/como-funciona" passHref>
                    <ListItem 
                        button 
                        component="a"
                    >
                        <ListItemIcon>
                            <SettingsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Cómo Funciona" />
                    </ListItem>
                </Link>
                <ListItem button>
                    <ListItemIcon>
                        <LiveHelpIcon />
                    </ListItemIcon>
                    <ListItemText primary="Ayuda" />
                </ListItem>
                <ListItem 
                    button 
                    component="a" 
                    href="https://www.facebook.com/PonteWApoEnElWhats"
                    target="_blank" 
                    rel="noopener"
                >
                    <ListItemIcon>
                        <FacebookIcon />
                    </ListItemIcon>
                    <ListItemText primary="Encuéntranos" />
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <MeetingRoomIcon />
                    </ListItemIcon>
                    <ListItemText primary="Salir" />
                </ListItem>
            </List>
            <Divider />
            <List>
                <ListItem>
                    <Link href="/negocios/registro" passHref>
                        <Button
                            component="a" 
                            startIcon={<ExploreIcon />}
                            color="primary"
                            variant="contained"
                        >
                            Descubre
                        </Button>
                    </Link>
                </ListItem>
            </List>
        </div>
    )
};

export default MenuItems

MenuItems.propTypes = {
    position: PropTypes.string,
    toggleDrawer: PropTypes.func,
}

import PropTypes from 'prop-types'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import MenuItems from './MenuItems'

const HamburgerMenu = ({ position }) => {
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (position, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [position]: open });
    };

    return (
        <>
            <IconButton
                    edge="end"
                    color="inherit"
                    aria-label="open drawer"
                    onClick={toggleDrawer(position, true)}
                >
                    <MenuIcon />
            </IconButton>
            <Drawer anchor={position} open={state[position]} onClose={toggleDrawer(position, false)}>
                <MenuItems position={position} toggleDrawer={toggleDrawer} />
            </Drawer>
        </>
        
    )
}

export default HamburgerMenu

HamburgerMenu.propTypes = {
    position: PropTypes.string,
}

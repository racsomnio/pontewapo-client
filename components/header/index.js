import {useState} from 'react'
import Link from 'next/link'

import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

import HamburgerMenu from './HamburgerMenu'

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      
    },
    toolbar: {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(2),
    },
    logo_box: {
      flexGrow: 1,
      alignSelf: 'center',
    },
    logo: {
        background: 'url(/nav-icons/ponte-wapo-bunny.svg)',
        width: '50px',
        height: '50px',
        display: 'block',
        position: 'relative',
        
        // '&:after': {
        //     content: "''",
        //     background: theme.palette.primary.main,
        //     width: '100%',
        //     height: '50%',
        //     position: 'absolute',
        //     bottom: '-5px',
        //     zIndex: '-1',
        //     borderRadius: '20px',
        //     right: '3px',
        // }
    }
}));

function Layout({home}) {
    const classes = useStyles();

    return (
        <div>
            <div>
                <Toolbar className={classes.toolbar}>
                    <div className={classes.logo_box}>
                        <Link href="/">
                            <a className={classes.logo}></a>
                        </Link>
                    </div>
                    <IconButton aria-label="search" color="inherit">
                        <SearchIcon />
                    </IconButton>
                    <HamburgerMenu position="right" />
                </Toolbar>
            </div>
        </div>
    )
}

export default Layout

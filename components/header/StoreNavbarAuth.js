import Link from 'next/link'
import ActiveLink from './ActiveLink'

function NavbarAuth({slug, full_menu, rights}) {
    const has_plan = !rights.includes('FREE');

    return (
        <>
            <ActiveLink activeClassName="nav-item__selected" href="/negocios/perfil">
                <a className="nav-item">
                    <img src="/icons/account.svg" alt="cuenta" />
                    <span>Editar Cuenta</span>
                </a>
            </ActiveLink>
            <ActiveLink activeClassName="nav-item__selected" href="/negocios/mi-menu">
                <a className="nav-item">
                    <img src="/icons/edit.svg" alt="agregar" />
                    <span>Editar Menú</span>
                </a>
            </ActiveLink>
            <ActiveLink activeClassName="nav-item__selected" href="/[meal]" href={`/${slug}`}>
                <a className="nav-item">
                    <img src="/icons/list.svg" alt="menús" />
                    <span>Ver Menú Digital</span>
                </a>
            </ActiveLink>
            {
                has_plan &&
                <ActiveLink activeClassName="nav-item__selected" href="/negocios/personalizar-menu">
                    <a className="nav-item">
                        <img src="/icons/brush.svg" alt="qr" />
                        <span>Personalizar Menú</span>
                    </a>
                </ActiveLink>
            }
            <ActiveLink activeClassName="nav-item__selected" href="/negocios/qr-code">
                <a className="nav-item">
                    <img src="/icons/scan.svg" alt="qr" />
                    <span>Mi Código QR</span>
                </a>
            </ActiveLink>
            <ActiveLink activeClassName="nav-item__selected" href="/negocios/ver-planes">
                <a className="nav-item">
                    <img src="/icons/upgrade.svg" alt="qr" />
                    <span>Ver Planes</span>
                </a>
            </ActiveLink>
        </>
    )
}

export default NavbarAuth

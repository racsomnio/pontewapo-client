import Link from 'next/link'
import ActiveLink from './ActiveLink'

function NavbarUnAuth() {
    return (
        <>
            <ActiveLink activeClassName="nav-item__selected" href="/ingresar" >
                <a className="nav-item">
                    <img src="/icons/unlocked.svg" alt="ingresar" />
                    <span>Ingresar</span>
                </a>
            </ActiveLink>
            <ActiveLink activeClassName="nav-item__selected" href="/negocios/registro" >
                <a className="nav-item">
                    <img src="/icons/old-keys.svg" alt="registrar" />
                    <span>Registra tu Negocio</span>
                </a>
            </ActiveLink>
            <ActiveLink activeClassName="nav-item__selected" href="/negocios/ver-planes">
                <a className="nav-item">
                    <img src="/icons/upgrade.svg" alt="qr" />
                    <span>Ver Planes</span>
                </a>
            </ActiveLink>
        </>
    )
}

export default NavbarUnAuth


import{ withApollo } from "../../utils/apollo"
import { useMutation } from "@apollo/react-hooks"
import { SIGNOUT, GET_CURRENT_USER } from '../../queries';

function Signout() {
    const [signout, { loading, error }] = useMutation(SIGNOUT, {
        refetchQueries: [{query: GET_CURRENT_USER}],
        awaitRefetchQueries: true,
    });
    
    function handleClick(e) {
        e.preventDefault();
        signout();
    }

    return (

        <a className="nav-item" onClick={handleClick}>
            <img src="/icons/logout.svg"/>
            <span>Salir</span>
        </a>
    )
}

export default withApollo()(Signout);
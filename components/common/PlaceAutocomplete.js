import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import styled from 'styled-components';
import useScript from '../../utils/useScript';
import Tooltip from '../common/Tooltip'

export default function PlaceAutocomplete({ addLocationToParentState }) {
    const [loaded, error] = useScript(
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyAuqm27yT8LDxPmlLNKvFXojAfW4EPa9UE&libraries=places'
    );
    const [ address, setAddress ] = React.useState('');
    let location = React.useRef({});

    const handleChange = address => {
        setAddress(address);
    };

    const handleSelect = address => {
        geocodeByAddress(address)
          .then(results => {
                return getLatLng(results[0])
            })
          .then(latLng => {
              const { lng, lat } = latLng;
              location.current={
                  address, 
                  coordinates: [lng, lat]
              };
            //   console.log(location.current);
              addLocationToParentState(location.current);
          })
          .catch(error => console.error('Error', error));
    };
    
    return !loaded ? <p>Loading</p> : (
            <PlacesAutocomplete
                value={address}
                onChange={handleChange}
                onSelect={handleSelect}
                debounce={300}
                ref={(c) => {
                    if (!c) return;
                    c.handleInputOnBlur = () => {};
                }}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <InputWrap>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message="Ej: Reforma 55, 06040"
                        >
                            <input
                            {...getInputProps({
                                placeholder: 'Dirección: Calle, Nº, C.P.',
                                className: 'location-search-input',
                            })}
                            />
                        </Tooltip>
                        <Dropdown>
                        {loading && <div style={{ textAlign: 'center' }}>...</div>}
                        {suggestions.map(suggestion => {
                            const className = suggestion.active
                            ? 'suggestion-item--active'
                            : 'suggestion-item';
                            return (
                                <div
                                    {...getSuggestionItemProps(suggestion, {
                                    className,
                                    })}
                                    key={suggestion.placeId}
                                >
                                    <span onClick={() => {
                                        setAddress(suggestion.description);
                                        return location.current = {
                                            ...location.current, 
                                            address: suggestion.description,
                                            placeId: suggestion.placeId
                                        }
                                    }}>
                                        {suggestion.description}
                                    </span>
                                </div>
                            );
                        })}
                        </Dropdown>
                    </InputWrap>
                )}
            </PlacesAutocomplete>
    )
}

const InputWrap = styled.div`
    position: relative;
`;

const Dropdown = styled.div`
    position: absolute;
    font-size: 0.9rem;
    color: rgb(117, 117, 117);
    width: 80%;
    top:2rem;
    text-align: left;
    max-width: 400px;
    left: 50%;
    transform: translateX(-50%);
    z-index: 2;
    background: #f2f2f2;
    border-radius: 10px;

    .suggestion-item, .suggestion-item--active{
        border-bottom: 1px solid rgba(250, 250, 250, 1);
        white-space: normal;
        cursor: pointer;
        
        span {
            padding: 1rem 1rem 1rem 3rem;
            display: block;
        }
    }

    .suggestion-item--active{
        font-weight: bold;
    }
`;

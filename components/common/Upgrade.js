import { useState } from 'react'
import { useMutation } from '@apollo/react-hooks';
import {loadStripe} from '@stripe/stripe-js';
import { CREATE_STRIPE_SESSION } from '../../queries';
import ModalLoginRedirect from './ModalLoginRedirect'



const Upgrade = ({children, plan}) => {
    const [modalState, toggleModal] = useState(false);
    const [createStripeSession, {error, loading}] = useMutation(CREATE_STRIPE_SESSION, {
        variables: { plan }
    })
    
    function closeModal() {
        toggleModal(false);
    }

    async function handleClick() {
        const res = await createStripeSession();
        const { message } = res.data.createStripeSession;
        if(!message) {
            toggleModal(true)
        }else{
            const stripe = await loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY);
            const {error} = await stripe.redirectToCheckout({
                sessionId: message
            })
        }

    }

    return (
            
        <ModalLoginRedirect isOpen={modalState} closeModal={closeModal}>
            <div onClick={handleClick}>
                {children}
            </div>
        </ModalLoginRedirect>
    )
}

export default Upgrade

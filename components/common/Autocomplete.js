import { useState } from 'react'
import styled from 'styled-components'

function Autocomplete({ suggestions, placeholder, defaultValue, callback }) {
    const suggestionsSorted = suggestions.sort((a, b) => {
        var nameA = removeAccents(a.type.toLowerCase()); 
        var nameB = removeAccents(b.type.toLowerCase());
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
      
        // names must be equal
        return 0;
    });

    const [values, setValues] = useState({
        // The active selection's index
        activeSuggestion: 0,
        // The suggestions that match the user's input
        filteredSuggestions: [],
        // Whether or not the suggestion list is shown
        showSuggestions: false,
        // What the user has typed
        userInput: defaultValue || "",
        userSelected: {}
    })

    function removeAccents(word) {
        return word.replace(/[á-ú]/g, x => {
            switch(x) {
                case "á":
                    return "a"
                    break;
                case "é":
                    return "e"
                    break;
                case "í":
                    return "i"
                    break;
                case "ó":
                    return "o"
                    break;
                case "ú":
                    return "u"
                    break;
            }
        })
    }

    function onChange(e) {
        const userInput = e.currentTarget.value;
    
        // Filter our suggestions that don't contain the user's input
        const filteredSuggestions = suggestionsSorted.filter(
            suggestion => {
                const noAccents = removeAccents(suggestion.type.toLowerCase());
                return noAccents.indexOf(userInput.toLowerCase()) > -1
            }
        );
    
        // Update the user input and filtered suggestions, reset the active
        // suggestion and make sure the suggestions are shown
        setValues({
          activeSuggestion: 0,
          filteredSuggestions,
          showSuggestions: true,
          userInput: e.currentTarget.value
        });
    };

    // Event fired when the user clicks on a suggestion
    function onClick(e, index) {
        // Update the user input and reset the rest of the state
        setValues({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: e.currentTarget.innerText,
            userSelected: values.filteredSuggestions[index]
        });

        callback({variables:{ food_type: values.filteredSuggestions[index] }})
    };

    // Event fired when the user presses a key down
    function onKeyDown(e) {
        const { activeSuggestion, filteredSuggestions } = values;

        // User pressed the enter key, update the input and close the
        // suggestions
        if (e.keyCode === 13) {
            setValues(prevState => ({
                ...prevState,
                activeSuggestion: 0,
                showSuggestions: false,
                userInput: filteredSuggestions[activeSuggestion].type,
                userSelected: filteredSuggestions[activeSuggestion]
            }));
            callback({variables:{ food_type: filteredSuggestions[activeSuggestion] }})
        }
        // User pressed the up arrow, decrement the index
        else if (e.keyCode === 38) {
            if (activeSuggestion === 0) {
                return;
            }

            setValues(prevState =>({ 
                ...prevState,
                activeSuggestion: activeSuggestion - 1 
            }));
        }
        // User pressed the down arrow, increment the index
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) {
                return;
            }

            setValues(prevState => ({ 
                ...prevState,
                activeSuggestion: activeSuggestion + 1 
            }));
        }
    };
    
    let suggestionsListComponent;

    if (values.showSuggestions && values.userInput) {
        if (values.filteredSuggestions.length) {
          suggestionsListComponent = (
            <ul className="suggestions">
              {values.filteredSuggestions.map((suggestion, index) => {
                let className;
  
                // Flag the active suggestion with a class
                if (index === values.activeSuggestion) {
                  className = "suggestion-active";
                }
  
                return (
                  <li
                    className={className}
                    key={suggestion.type}
                    onClick={e => onClick(e, index)}
                  >
                    {suggestion.type}
                  </li>
                );
              })}
            </ul>
          );
        } else {
          suggestionsListComponent = (
            <div className="no-suggestions">
              <em>Selecciona una de las opciones</em>
            </div>
          );
        }
    }

    return (
        <AutocompleteWrap>
            <input
                type="text"
                onChange={onChange}
                onKeyDown={onKeyDown}
                value={values.userInput}
                placeholder={placeholder}
                name={name}
            />
            {suggestionsListComponent}
        </AutocompleteWrap>
    )
}

export default Autocomplete;

const AutocompleteWrap = styled.div`
    position: relative;
    width: fit-content;
    margin: auto;
    
    .no-suggestions {
        color: #999;
        padding: 0.5rem;
    }

    .suggestions {
        border: 1px solid #ccc;
        border-top-width: 0;
        list-style: none;
        max-height: 14rem;
        overflow-y: auto;
        padding-left: 0;
        width: 200px;
        position: absolute;
        margin: 0;
        top: 2.5rem;
        left: 50%;
        transform: translateX(-50%);
        background: #fff;
        z-index: 2;
        border-radius: 0 0 10px 10px;
        box-shadow: 0 0 20px #ccc;

        li {
            padding: 0.5rem;
        }
    }

    .suggestion-active,
    .suggestions li:hover {
        background-color: #7a1a7b;
        color: #fff;
        cursor: pointer;
        font-weight: 700;
    }

    .suggestions li:not(:last-of-type) {
        border-bottom: 1px solid #999;
    }
`;

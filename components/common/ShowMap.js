import styled from 'styled-components';
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import L from 'leaflet';

const Fixed = styled.div`
    position: fixed;
    background: #fff;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index:103;
`;

const Close = styled.span`
    position: absolute;
    right: 2rem;
    top: 2rem;
    color: #fff;
    background: #7a1a7b;
    border-radius: 3rem;
    z-index: 401;
    padding: 1rem;
    font-size: 0.8rem;
    cursor: pointer;
`;

const tupper = new L.Icon({
    iconUrl: '/tupperPin.svg',
    iconRetinaUrl: '/tupperPin.svg',
    popupAnchor: [10, -44],
    iconSize: [55, 55],
    iconAnchor: [27.5, 55],
})

const BusinessMap = ({location, close}) => {
    const { address, coordinates } = location;

    return (
        <Fixed>
            <Close onClick={close}>
                Cerrar Mapa
            </Close>
            <LeafletMap
                center={[coordinates[1], coordinates[0]]}
                zoom={16}
                maxZoom={18}
                attributionControl={true}
                zoomControl={true}
                doubleClickZoom={true}
                scrollWheelZoom={true}
                dragging={true}
                animate={true}
                easeLinearity={0.35}
            >
                <TileLayer
                url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                />
                <Marker 
                    position={[coordinates[1], coordinates[0]]}
                    icon={tupper}
                >
                    <Popup>
                        {address}
                    </Popup>
                </Marker>
            </LeafletMap>
        </Fixed>
    )
}

export default BusinessMap

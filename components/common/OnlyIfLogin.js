import { useQuery } from "@apollo/react-hooks"
import { GET_CURRENT_USER } from '../../queries';
import Link from 'next/link'
import { useRouter } from 'next/router'

function OnlyIfLogin({ children }) {
    const { loading, error, data, fetchMore } = useQuery(GET_CURRENT_USER);
    const router = useRouter()
    const { asPath } = router;

    if(loading) return null
    if(error) return null
    
    return (
        <div>
            {
                !data.getCurrentUser
                ? <p>
                    <small>Parece que te has salido de la oficina.</small>
                    <br/>
                    <small>Accede a tu cuenta con tu correo y contraseña</small>
                    <br/><br/>
                    <Link href={`/ingresar?redirect=${asPath}`}>
                        <a className="button">Ingresar</a>
                    </Link>
                </p>
                : children
            }
        </div>
    )
}

export default OnlyIfLogin

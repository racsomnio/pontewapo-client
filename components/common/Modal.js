import styled from 'styled-components'

function Modal({ children, isOpen, closeModal, styles, full }) {
    const defaultStyles = {
        modalBackground: '#fff',
        closeBtn: '#7a1a7b',
        ...styles
    };

    return (
        <>    
            <BgModal 
                open={isOpen} 
                onClick={closeModal}
            />
            <ToogleModal 
                open={isOpen}
                overwrite={defaultStyles.modalBackground}
                full={full}
            >
                <Close 
                    onClick={closeModal} 
                    overwrite={defaultStyles.closeBtn}
                >
                    X
                </Close>
                <div className="modal-body">
                    {children}
                </div>
            </ToogleModal>
        </>
    )
}

const ToogleModal = styled.div`
    display: ${ ({open}) => open ? 'block' : 'none' };
    position: fixed;
    z-index: 2;
    left: 50%;
    top: 50%;
    transform: translate(-50%,-50%);
    width: 90%;
    max-width: 500px;
    opacity: ${ ({open}) => open ? '1' : '0' };
    background: ${ ({overwrite}) => overwrite};
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    box-shadow: 0 0 10px 5px rgba(0,0,0,0.1);
    padding: 1rem 2rem 2rem;
    bottom: ${({full}) => full ? '-44%' : 'none'};
    overflow-y:scroll;
    overflow-x:hidden;
`;

const Close = styled.span`
    position: absolute;
    top: 0;
    left:0;
    width: 30px;
    height: 30px;
    background: ${ ({overwrite}) => overwrite};
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    display: flex;
    justify-content:center;
    align-items: center;
    color: #fff;
    font-weight: bold;
    font-size: 0.75rem;
    cursor: pointer;
`;

const BgModal = styled.div`
    display: ${ ({open}) => open ? 'block' : 'none' };
    position: fixed;
    z-index: 2;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.8);
    left: 0;
    top: 0;
`;

export default Modal

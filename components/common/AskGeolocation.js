import { useState, useEffect } from 'react'

const GetGeolocation = ({children, coords, updateCoords}) => {
    const [coordinates, setCoordinates] = useState(localStorage.getItem('coords'));

    useEffect( () => {
        if(coords) setCoordinates(coords);
    }, [coords])

    const watchPosition = () => {
        navigator.geolocation.getCurrentPosition(
            pos => posSuccess(pos),
            err => console.warn('ERROR(' + err.code + '): ' + err.message),
            {maximumAge:10000, timeout:5000, enableHighAccuracy: true}
        )
    }

    const posSuccess = (pos) => {
        const concatCoords= `${pos.coords.longitude},${pos.coords.latitude}`;
        localStorage.setItem('coords', concatCoords);
        setCoordinates(concatCoords);
        if(updateCoords) updateCoords(concatCoords);
    }

    if(!coordinates) return (
        <button onClick={watchPosition}>
           Activa mi ubicación 
        </button>
    )

    return !!children ? children : null
}

export default GetGeolocation

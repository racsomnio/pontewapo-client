import Link from 'next/link'
import Modal from './Modal'

const ModalLoginRedirect = ({children, isOpen, closeModal}) => {
    return (
        <div>
            {children}
            
            <Modal isOpen={isOpen} closeModal={closeModal}>
                <p>Por favor registrate ó ingresa a tu cuenta</p>
                <div style={{ marginBottom: '1rem' }}>
                    <Link href="/negocios/registro">
                        <a className="button secondary">Crea Una Cuenta</a>
                    </Link>
                </div>
                <div>
                    <Link href="/ingresar">
                        <a className="button">Ingresar</a>
                    </Link>
                </div>
            </Modal>
        </div>
    )
}

export default ModalLoginRedirect

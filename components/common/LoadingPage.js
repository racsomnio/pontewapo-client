import styled from 'styled-components'
import CircularProgress from '@material-ui/core/CircularProgress';

function LoadingPage() {
    return (
        <Loader>
            <CircularProgress />
        </Loader>
    )
}

const Loader = styled.div`
    opacity: 0;
    height: 0;
    transition: height, opacity 0.5s;
    position: fixed;
    z-index: 1023;
    background: rgba(255,255,255, 0.8);
    width: 100%;
    bottom: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    left: 0;

    .loading-page & {
        height: 100vh;
        opacity: 1;
    }
`;

export default LoadingPage

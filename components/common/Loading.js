import styled, { keyframes }  from 'styled-components';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Loader = styled.span`
    border: ${props => (props.invert ? '12px solid #7a1a7b' : '12px solid #f3f3f3')};
    border-radius: 50%;
    border-top: 0px solid;
    width: 25px;
    height: 25px;
    display: inline-block;
    vertical-align: middle;
    margin-top: -5px;
    animation: ${rotate} 2s linear infinite;    
`;

export default function Loading({invert}) {
    return (
        <Loader invert={invert}/>
    )
}


const Error = ({error}) => <p>{error.message.replace("GraphQL error: ", '')}</p>

export default Error;

import Link from 'next/link';
import { withTheme } from "@material-ui/core/styles"
import styled from 'styled-components';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import StorefrontIcon from '@material-ui/icons/Storefront';
import FacebookIcon from '@material-ui/icons/Facebook';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MuiLink from '@material-ui/core/Link';

const FooterBox = withTheme(styled('footer')`
    background: #000;
    color: #fff;
    padding: ${({theme}) => theme.spacing(4, 2, 5)};
`);

const Footer = () => {
    return (
        <FooterBox>
            <Grid container spacing={3}>
            <Grid item xs={12} sm={4}>
                    <Box textAlign="center">
                        <Link href="/negocios/registro" passHref>
                        <Button
                            component="a" 
                            startIcon={<StorefrontIcon />}
                            color="primary"
                            variant="outlined"
                        >
                            Registra Tu Negocio
                        </Button>
                        </Link>
                    </Box>
                </Grid>

                <Grid item container justify="flex-end" xs={12} sm={8} spacing={0, 2}>
                    <Grid item xs={6}>
                        <Typography align="center">
                            <Link href="/negocios/registro" passHref>
                                <MuiLink color="secondary">
                                    Ayuda
                                </MuiLink>
                            </Link>
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography align="center">
                            <Link href="/negocios/registro" passHref>
                                <MuiLink color="secondary">
                                    Cómo Funciona
                                </MuiLink>
                            </Link>
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography align="center">
                            <Link href="/negocios/registro" passHref>
                                <MuiLink color="secondary">
                                    Contáctanos
                                </MuiLink>
                            </Link>
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography align="center">
                            <MuiLink color="secondary" href="https://www.facebook.com/PonteWApoEnElWhats">
                                <FacebookIcon />
                            </MuiLink>
                        </Typography>
                    </Grid>
                </Grid>

                
                <Grid item xs={12}>
                    <Typography align="center">Hecho con ❤️ desde NY</Typography>
                </Grid>

                <Grid item xs={12} alignContent="center" container justify="center" spacing={1, 3}>
                    <Grid item>
                        <Link href="/terminos-y-condiciones" passHref>
                            <MuiLink>
                                Términos y Condiciones
                            </MuiLink>
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link href="/aviso-de-privacidad" passHref>
                            <MuiLink>
                                Aviso de Privacidad
                            </MuiLink>
                        </Link>
                    </Grid>
                </Grid>
            </Grid>
        </FooterBox>
    )
}

export default Footer

export const handleWindowSize = (el, callback) => {
    // console.log('handleWindowSize called');
    
    const width = window.innerWidth;
    if(width >= 768){
      const content = document.querySelectorAll(el);
      const contentWidth = content[content.length - 1].offsetWidth;
      
      callback(contentWidth+'px');
    }else{
      callback('');
    }
  
  };
  
export const convertTo64base = (e, field, callback) => {
  e.persist();
  const FR= new FileReader();
  FR.addEventListener("load", async function(event) {
      // console.log( event.target.result );
      const res = await callback({
          variables: { img64base: event.target.result, flag: field }
      });
      // console.log(res);
  }); 
    
  FR.readAsDataURL( e.target.files[0] );
}
  
export const swap_it = {
  desayunos: 'breakfast',
  comidas: 'lunch',
  cenas: 'dinner',
  bebidas: 'drinks',
  postres: 'dessert',
  antojitos: 'snacks',
  tragos: 'beverages'
}

export const getMealInSpanish = (meal, mealList) => {
  const keys = Object.keys(mealList).filter((k) => {
      if(swap_it[k] == meal) return k
  });
  return keys[0];
}
  
export const days = {
  0: 'Dom',
  1: 'Lun',
  2: 'Mar',
  3: 'Mie',
  4: 'Jue',
  5: 'Vie',
  6: 'Sab',
}

export const daysLong = {
  0: 'Domingo',
  1: 'Lunes',
  2: 'Martes',
  3: 'Miércoles',
  4: 'Jueves',
  5: 'Viernes',
  6: 'Sábado',
}

export const countries = [
  { code: 'MX', label: 'México', phone: '52', type: 'Mexicana' },
  { code: 'US', label: 'Estados Unidos', phone: '1', type: 'Americana' },
  { code: 'CA', label: 'Canada', phone: '1', type: 'Canadiense'},
  { code: 'AR', label: 'Argentina', phone: '54', type: 'Argentina' },
  { code: 'CL', label: 'Chile', phone: '56', type: 'Chilena' },
  { code: 'BZ', label: 'Belice', phone: '501', type: 'Beliceña' },
  { code: 'BO', label: 'Bolivia', phone: '591', type: 'Boliviana' },
  { code: 'BR', label: 'Brasil', phone: '55', type: 'Brasileña' },
  { code: 'CO', label: 'Colombia', phone: '57', type: 'Colombiana' },
  { code: 'CR', label: 'Costa Rica', phone: '506', type: 'Costarricense' },
  { code: 'CU', label: 'Cuba', phone: '53', type: 'Cubana' },
  { code: 'EC', label: 'Ecuador', phone: '593', type: 'Ecuatoriana' },
  { code: 'SV', label: 'El Salvador', phone: '503', type: 'Salvadoreña' },
  { code: 'ES', label: 'España', phone: '34', type: 'Española' },
  { code: 'GT', label: 'Guatemala', phone: '502', type: 'Guatemalteca' },
  { code: 'HT', label: 'Haiti', phone: '509', type: 'Haitiana' },
  { code: 'HN', label: 'Honduras', phone: '504', type: 'Hondureña' },
  { code: 'PA', label: 'Panamá', phone: '507', type: 'Panameña' },
  { code: 'PY', label: 'Paraguay', phone: '595', type: 'Paraguaya' },
  { code: 'PE', label: 'Perú', phone: '51', type: 'Peruana' },
  { code: 'PR', label: 'Puerto Rico', phone: '1', type: 'Puertorriqueña' },
  { code: 'DO', label: 'República Dominicana', phone: '1-809', type: 'Dominicana' },
  { code: 'VE', label: 'Venezuela', phone: '58', type: 'Venezolana' },
  { code: 'UY', label: 'Uruguay', phone: '598', type: 'Uruguaya' },

  { code: 'DE', label: 'Alemania', phone: '49', type: 'Alemana'  },
  { code: 'CN', label: 'China', phone: '86', type: 'China' },
  { code: 'FR', label: 'Francia', phone: '33', type: 'Francesa' },
  { code: 'GR', label: 'Grecia', phone: '30', type:'Griega' },
  { code: 'JP', label: 'Japón', phone: '81', type:'Japonesa' },
  { code: 'IT', label: 'Italia', phone: '39', type: 'Italiana' },
  { code: '--', label: 'África', phone: 'NA', type: 'Africana' },
  { code: '--', label: 'Asia', phone: 'NA', type: 'Asiática' },
  { code: '--', label: 'Caribe', phone: 'NA', type: 'Caribeña' },
  { code: '--', label: 'Mediterraneo', phone: 'NA', type: 'Mediterranea' },
  { code: '--', label: 'Internacional', phone: 'NA', type: 'Internacional' },
  { code: '--', label: 'Europa', phone: 'NA', type: 'Europea' },
  { code: '--', label: 'Árabia', phone: '971', type: 'Árabe' },
  { code: '--', label: 'Vegana', phone: '971', type: 'Vegana' },
  { code: '--', label: 'Vegetariana', phone: '971', type: 'Vegetariana' },
  { code: '--', label: 'Exótica', phone: '971', type: 'Exótica' },
  { code: 'AT', label: 'Austria', phone: '43', type: 'Austriaca' },
  { code: 'AU', label: 'Australia', phone: '61', type: 'Australiana' },
  { code: 'BE', label: 'Belgica', phone: '32', type: 'Belga'  },
  { code: 'BG', label: 'Bulgaria', phone: '359', type: 'Búlgara'  },
  { code: 'CZ', label: 'Republica Checa', phone: '420', type: 'Checa' },
  { code: 'DK', label: 'Dinamarca', phone: '45', type: 'Danesa' },
  { code: 'EG', label: 'Egipto', phone: '20', type: 'Egipcia' },
  { code: 'FI', label: 'Finlandia', phone: '358', type:'Finlandesa' },
  { code: 'GB', label: 'Inglaterra', phone: '44', type:'Inglesa' },
  { code: 'HK', label: 'Hong Kong', phone: '852', type:'Hongkonesa' },
  { code: 'HR', label: 'Croatia', phone: '385', type: 'Croata' },
  { code: 'HU', label: 'Hungria', phone: '36', type:'Húngara' },
  { code: 'ID', label: 'Indonesia', phone: '62', type:'Indonesia' },
  { code: 'IE', label: 'Irlandia', phone: '353', type:'Irlandesa' },
  { code: 'IL', label: 'Israel', phone: '972', type:'Israelita' },
  { code: 'IN', label: 'India', phone: '91', type: 'Hindú' },
  { code: 'IQ', label: 'Iraq', phone: '964', type: 'Iraní' },
  { code: 'IS', label: 'Islandia', phone: '354', type:'Islandesa' },
  { code: 'JM', label: 'Jamaica', phone: '1-876', type:'Jamaiquina' },
  { code: 'KH', label: 'Camboya', phone: '855', type:'Camboyana' },
  { code: 'KR', label: 'Corea', phone: '82', type:'Coreana' },
  { code: 'LB', label: 'Líbano', phone: '961', type:'Libanesa' },
  { code: 'MA', label: 'Morruecos', phone: '212', type:'Marrueca' },
  { code: 'MM', label: 'Myanmar', phone: '95', type:'Birmana' },
  { code: 'MN', label: 'Mongolia', phone: '976', type:'Mongola' },
  { code: 'MY', label: 'Malasia', phone: '60', type:'Malaya' },
  { code: 'NI', label: 'Nicaragua', phone: '505', type:'Nicaraguense' },
  { code: 'NL', label: 'Holanda', phone: '31', type:'Holandesa' },
  { code: 'NO', label: 'Noruega', phone: '47', type:'Noruega' },
  { code: 'NZ', label: 'Nueva Zelanda', phone: '64', type:'Neozelandeza' },
  { code: 'PH', label: 'Filipinas', phone: '63', type:'Filipina' },
  { code: 'PK', label: 'Pakistan', phone: '92', type:'Pakistaní' },
  { code: 'PL', label: 'Polonia', phone: '48', type:'Polaca' },
  { code: 'PT', label: 'Portugal', phone: '351', type:'Portuguesa' },
  { code: 'RO', label: 'Rumania', phone: '40', type:'Rumana' },
  { code: 'RU', label: 'Rusia', phone: '7', type:'Rusa' },
  { code: 'SE', label: 'Suecia', phone: '46', type:'Sueca' },
  { code: 'SG', label: 'Singapur', phone: '65', type:'Singapurense' },
  { code: 'SI', label: 'Elovenia', phone: '386', type:'Eslovena' },
  { code: 'SK', label: 'Elovaquia', phone: '421', type:'Eslovaca' },
  { code: 'TH', label: 'Tailandia', phone: '66', type:'Tailandesa' },
  { code: 'TR', label: 'Turquía', phone: '90', type:'Turca' },
  { code: 'TW', label: 'Táiwan', phone: '886', type:'Taiwanesa' },
  { code: 'UA', label: 'Ucrania', phone: '380', type:'Ucraniana' },
  { code: 'VN', label: 'Vietnam', phone: '84', type:'Vietnamita' },
  { code: 'ZA', label: 'Sudáfrica', phone: '27', type:'Sudafricana' },
];

// ISO 3166-1 alpha-2
// ⚠️ No support for IE 11
export function countryToFlag(isoCode) {
  if (isoCode === '--') return null;
  return typeof String.fromCodePoint !== 'undefined'
    ? isoCode
        .toUpperCase()
        .replace(/./g, (char) => String.fromCodePoint(char.charCodeAt(0) + 127397))
    : isoCode;
}

export const specialities = [
  'Aguachile',
  'Alitas',
  'Arepas',
  'Antojitos Mexicanos',
  'Arrachera',
  'Bacalao',
  'Baile',
  'Bandeja Paisa',
  'Barbacoa',
  'Bibimbap',
  'Birria',
  'Cabrito',
  'Calzone',
  'Cangrejo',
  'Camarones',
  'Cantaritos',
  'Capirotada',
  'Carnes',
  'Carne Asada',
  'Carne Kobe',
  'Carpaccio',
  'Cecina',
  'Cerveza Importada',
  'Ceviche',
  'Chocolate',
  'Churrasco',
  'Cochinita Pibil',
  'Coctel de Camarones',
  'Consomé',
  'Clericot',
  'Cocteles',
  'Crepas',
  'Croissant',
  'Carnitas',
  'Chapulines',
  'Chilaquiles',
  'Chile Poblano',
  'Chimichurri',
  'Dim Sum',
  'Dumplings',
  'Empanadas',
  'Ensaladas',
  'Esquites',
  'Feijoada',
  'Frijoles con veneno',
  'Gorditas',
  'Guacamole',
  'Hamburguesas',
  'Helados',
  'Hot Dogs',
  'Huitlacoche',
  'Jericalla',
  'Kimchi',
  'Langosta',
  'Lasaña',
  'Machaca',
  'Mariscos',
  'Mate',
  'Michelada',
  'Milanesas',
  'Mole',
  'Mondongo',
  'Música',
  'Okonomiyaki',
  'Ostiones',
  'Paella',
  'Pad Thai',
  'Papadzules',
  'Pasta',
  'Pastel de Cumpleaños',
  'Pastel de Nata',
  'Pastor',
  'Pescado',
  'Pescado a la brasa',
  'Pho',
  'Pizza',
  'Poc Chuc',
  'Pozole',
  'Pollo',
  'Pollo Tandoori',
  'Pulque',
  'Pupusas',
  'Quesadillas',
  'Quesos',
  'Ramen',
  'Rissotto',
  'Ropa Vieja',
  'Salchichas',
  'Sake',
  'Salmón',
  'Sancocho',
  'Sándwich',
  'Sándwich cubano',
  'Sashimi',
  'Spaguetti',
  'Sopa de tortilla',
  'Sushi',
  'Table Dance',
  'Tacos',
  'Tacos Árabes',
  'Tacos de Canasta',
  'Tapas',
  'Té',
  'Tempura',
  'Tequila',
  'Teriyaki',
  'Tiramisú',
  'Tlayudas',
  'Tortas',
  'Torta Ahogada',
  'Tamales',
  'Tamales Oaxaqueños',
  'Udon',
  'Vinos',
  'Vuelve a la Vida',
  'Wagyu',
  'Whisky',
]


export function removeAccents(word) {
  return word.replace(/[á-ú]/g, x => {
      switch(x) {
          case "á":
              return "a"
              break;
          case "é":
              return "e"
              break;
          case "í":
              return "i"
              break;
          case "ó":
              return "o"
              break;
          case "ú":
              return "u"
              break;
      }
  })
}
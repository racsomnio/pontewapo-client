import { ApolloClient } from 'apollo-client'
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import fetch from 'isomorphic-unfetch'
import introspectionQueryResultData from './fragmentTypes.json';

export default function createApolloClient(initialState, ctx) {
  // Use instrospection in order to be able to use Union Type in graphql,
  // Needed to query type Owner 
  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData
  });
  
  const cache = new InMemoryCache({
    fragmentMatcher,
    addTypename: false
  }).restore(initialState)

  // The `ctx` (NextPageContext) will only be present on the server.
  // use it to extract auth headers (ctx.req) or similar.
  return new ApolloClient({
    ssrMode: Boolean(ctx),
    link: new HttpLink({
      uri: process.env.ENDPOINT, // Server URL (must be absolute)
      credentials: 'include', // Additional fetch() options like `credentials` or `headers`
      fetch,
    }),
    cache
  })
}
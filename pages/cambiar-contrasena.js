import {useState} from 'react'
import Router, { useRouter } from 'next/router'
import { withApollo } from "../utils/apollo";
import { useMutation } from "@apollo/react-hooks"
import styled from 'styled-components'
import {RESET_PASSWORD} from '../queries';
import Error from '../components/common/Error'

const initialState = {
    password: "",
}

const Ingresar = () => {
    const router = useRouter()
    const { reset } = router.query;

    const [formValues, setFormValues] = useState({
        resetToken: reset,
        password: "",
    })

    function handleChange(e) {
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            [name]: value
        }));
    }

    function handleSubmit(e) {
        e.preventDefault();
        resetPassword().then(async({data}) => {
            Router.push(`/ingresar`); 
        });
    }
    
    function validateForm() {
        const {password} = formValues;
        const isInvalid = !password;
        return isInvalid;
    }

    const [resetPassword, {error, loading}] = useMutation(RESET_PASSWORD, {
        variables: formValues
    });

    return (
        <>
            <h1>Cambia tu Contraseña</h1>

            <form className="ui form" onSubmit={handleSubmit}>
                {/* <FieldWrap>
                    <PasswordEye 
                        name="password" 
                        placeholder="Contraseña" 
                        onChange={handleChange} 
                        value={formValues.password} 
                    />
                </FieldWrap> */}

                <FieldWrap double>
                    <button 
                        className="ui button" 
                        type="submit"
                        disabled={loading || validateForm()}
                    >Cambiar</button>
                    { error && <Error error={error}/> }
                </FieldWrap>
            </form>
        </>
    )
}

const FieldWrap = styled.div`
    margin-bottom: ${ ({double}) => double ? '3rem' : '1.5rem' };
`;

export default withApollo({ ssr: true })(Ingresar);

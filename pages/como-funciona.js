import Link from 'next/link'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider'
import StorefrontIcon from '@material-ui/icons/Storefront';
import Grid from '@material-ui/core/Grid'
import MoneyOffIcon from '@material-ui/icons/MoneyOff';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import FreeBreakfastIcon from '@material-ui/icons/FreeBreakfast';
import SmsIcon from '@material-ui/icons/Sms';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  primary: {
    // color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.main,
  },
  largeAvatar: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    background: theme.palette.terciary.main,
  },
}));

function HowItWorks() {
    const classes = useStyles();
    return (
        <Container maxWidth="md">
            <h1>Cómo Funciona</h1>

            <Box>
              <p>Ponte WApo te ayuda a crear una tienda en línea para que te anuncies y recibas pedidos a través de WhatsApp y sin cargos.</p>
              <p>Deja atrás los grupos de WhatsApp y deja que te ecuentren sólo los clientes que estan cerca de ti.</p>
              <img src="/home/like-wa.svg" alt="ponte wapo en whatsapp"/> 
            </Box>

            <Box mt={4}>
              <h2>Pasos para aunciarte en PonteWApo:</h2>
              <List>
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                      <Avatar className={classes.primary}>
                        1
                      </Avatar>
                  </ListItemAvatar>
                  <ListItemText 
                    primary="Regístrate" 
                    secondary="Sólo necesitas tu correo electrónico" 
                  />
                </ListItem>
                <Divider variant="inset" component="li" />
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                      <Avatar className={classes.primary}>
                        2
                      </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary="Configura Tu Negocio"
                    secondary="Dinos como deseas recibir pedidos"
                  />
                </ListItem>
                <Divider variant="inset" component="li" />
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                      <Avatar className={classes.primary}>
                        3
                      </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary="Nosotros digitalizamos tu catálogo"
                    secondary="Sube tu menú, catalogo o lista de productos y nosotros nos encargamos de digitalizarlos."
                  />
                </ListItem>
                <Divider variant="inset" component="li" />
                <ListItem alignItems="flex-start">
                  <ListItemAvatar>
                      <Avatar className={classes.primary}>
                        4
                      </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary="Agrega Fotos De Tus Productos"
                    secondary="Una vez digitalizado, te envíamos un mensaje para que revises tu catálogo y agregues las fotos que quieras"
                  />
                </ListItem>
              </List>

              <Box mt={4}>
                <Link href="/negocios/registro" passHref>
                    <Button
                        component="a" 
                        startIcon={<StorefrontIcon />}
                        color="primary"
                        variant="contained"
                        size="large"
                    >
                        Registra Tu Negocio
                    </Button>
                </Link>
              </Box>
            </Box>

            <Box mt={8}>
                <h2>¿Por que anunciarse en PonteWApo?</h2>
                <Grid container>
                  <Grid 
                    item
                    container
                    alignItems="center"
                    direction="column"
                  >
                    <Avatar className={classes.largeAvatar}>
                      <MoneyOffIcon fontSize="large" />
                    </Avatar>
                    <Box mt={2} mb={5}>
                      <Typography>No necesitas pagar para anunciarte</Typography>
                    </Box>
                  </Grid>

                  <Grid 
                    item
                    container
                    alignItems="center"
                    direction="column"
                  >
                    <Avatar className={classes.largeAvatar}>
                      <LocationOnIcon fontSize="large"/>
                    </Avatar>
                    <Box mt={2} mb={5}>
                      <Typography>Deja que clientes cerca de ti te ecuentren.</Typography>
                    </Box>
                  </Grid>

                  <Grid 
                    item
                    container
                    alignItems="center"
                    direction="column"
                  >
                    <Avatar className={classes.largeAvatar}>
                      <FreeBreakfastIcon fontSize="large"/>
                    </Avatar>
                    <Box mt={2} mb={5}>
                      <Typography>Tú sólo te encargas de recibir pedidos, nosotros nos encargamos de que te encuentren</Typography>
                    </Box>
                  </Grid>

                  <Grid 
                    item
                    container
                    alignItems="center"
                    direction="column"
                  >
                    <Avatar className={classes.largeAvatar}>
                      <SmsIcon fontSize="large"/>
                    </Avatar>
                    <Box mt={2} mb={5}>
                      <Typography>Recibe tus pedidos en un sólo mensaje</Typography>
                    </Box>
                  </Grid>
                </Grid>
            </Box>

            <Box  mt={6}>
              <h2>¿Ya estás haciendo pedidos por WhatsApp con otra compañía?</h2>
              <Typography align="left" paragraph>En PonteWApo creemos que el sol sale para todos y no queremos que dejes lo que ya tienes.</Typography>
              <Typography align="left" paragraph>Nosotros somos una opción mas que te ayuda a generar más ingresos.</Typography>
              <Typography align="left" paragraph><strong>Probarlo no te costará nada, al contrario, te dará mas oportunidades.</strong> 🙂</Typography>
            </Box>
        </Container>
    )
}

export default HowItWorks

import {useState} from 'react'
import Router from 'next/router'
import { withApollo } from "../utils/apollo";
import { useMutation } from "@apollo/react-hooks"
import styled from 'styled-components'
import { REQUEST_RESET } from '../queries';
import Error from '../components/common/Error'

import { string, object } from 'yup';
import Container from '@material-ui/core/Container'
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box'
import Tooltip from '@material-ui/core/Tooltip'
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'
import TelegramIcon from '@material-ui/icons/Telegram';

const initialState = {
    email: "",
}

const validationSchema = object().shape({
    email: string().email('Este correo no es válido'),
});

const ForgotPasswordPage = () => {
    const [formValues, setFormValues] = useState(initialState)
    const [ errors, setErrors ] = useState({});

    function handleChange(e) {
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            [name]: value
        }));
    }

    async function validateAllInputs (e) {
        e.preventDefault();
        await validationSchema.validate(
            formValues,
            // { abortEarly: false }
        ).then(valid => {
            console.log('valid');
            setErrors({})
            handleSubmit(e);
          }).catch(err => {
            console.log('err:', err)
            setErrors(err)
        })
    }

    function handleSubmit(e) {
        e.preventDefault();
        requestReset().then(async({data}) => {
            clearState();
            Router.push(`/revisa-tu-email?email=${formValues.email}`); 
        });
    }

    function clearState() {
        setFormValues({...initialState});
    }
    
    function validateForm() {
        const {email} = formValues;
        const isInvalid = !email;
        return isInvalid;
    }

    const [requestReset, {error, loading}] = useMutation(REQUEST_RESET, {
        variables: formValues,
    });

    return (
        <Container maxWidth="sm">
            <h1>Olvidé Contraseña</h1>

            <form onSubmit={validateAllInputs}>
            <Box mb={4}>
                    <Tooltip 
                        title={errors.message || ""} 
                        arrow
                        open={errors.path === 'email' && !!errors.errors[0]}
                        placement="top"
                    >
                        <TextField 
                            id="input-with-icon-grid"
                            type="tel"
                            style={{ width: 250 }}
                            variant="outlined"
                            label="Email" 
                            placeholder="Tu Correo Eletrónico"
                            name="email"
                            onChange={handleChange}
                            InputProps={{
                                startAdornment: (
                                <InputAdornment position="start">
                                    <MailOutlineIcon />
                                </InputAdornment>
                                ),
                            }}
                            error={errors.path === 'email' && !!errors.errors[0]}
                        />
                    </Tooltip>
                </Box>

                <Box mt={4}>
                    <Button
                        endIcon={<TelegramIcon />}
                        variant="contained"
                        color="primary"
                        size="large"
                        disabled={loading}
                        type="submit"
                        disabled={loading || validateForm()}
                    >
                        Enviar 
                    </Button>
                </Box>

                {/* <FieldWrap>
                    <input type="email" name="email" placeholder="Email" onChange={handleChange} value={formValues.email} />
                </FieldWrap>

                <FieldWrap double>
                    <button 
                        className="ui button" 
                        type="submit"
                        disabled={loading || validateForm()}
                    >Enviar</button>
                    { error && <Error error={error}/> }
                </FieldWrap> */}
            </form>
        </Container>
    )
}

export default withApollo({ ssr: true })(ForgotPasswordPage);

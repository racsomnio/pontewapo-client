import {useState, useEffect} from 'react'
import Router, { useRouter } from 'next/router'
import Link from 'next/link'
import { string, object } from 'yup';
import { withApollo } from "../utils/apollo";
import { useMutation } from "@apollo/react-hooks"
import styled from 'styled-components'
import {SINGNIN_USER, GET_CURRENT_USER} from '../queries';
import Error from '../components/common/Error'


import Container from '@material-ui/core/Container'
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box'
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import Button from '@material-ui/core/Button'
import InputAdornment from '@material-ui/core/InputAdornment'
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton'
import FormControl from '@material-ui/core/FormControl'
import Tooltip from '@material-ui/core/Tooltip'
import TelegramIcon from '@material-ui/icons/Telegram';

const initialState = {
    email: "",
    password: "",
}

const validationSchema = object().shape({
    email: string().email('Este correo no es válido').required('Tu correo es necesario'),
    password: string().required('Escribe tu contraseña'),
});

const Ingresar = () => {
    const router = useRouter()
    const { redirect } = router.query;

    const [formValues, setFormValues] = useState(initialState)
    const [ errors, setErrors ] = useState({});
    const [ showPassword, setShowPassword ] = useState(false)

    function handleChange(e) {
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            [name]: value
        }));
    }

    function clearState() {
        setFormValues({...initialState});
    }
    
    function validateForm() {
        const {email, password} = formValues;
        const isInvalid = !email || !password;
        return isInvalid;
    }

    const [signinUser, {error, loading}] = useMutation(SINGNIN_USER, {
        variables: formValues,
        refetchQueries: [{query: GET_CURRENT_USER}],
        // awaitRefetchQueries: true,
    });

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    async function validateAllInputs (e) {
        e.preventDefault();
        await validationSchema.validate(
            formValues,
            // { abortEarly: false }
        ).then(valid => {
            console.log('valid');
            setErrors({})
            handleSubmit(e);
          }).catch(err => {
            console.log('err:', err)
            setErrors(err)
        })
    }

    function handleSubmit(e) {
        e.preventDefault();
        signinUser().then(async({data}) => {
            clearState();
            Router.push(`${redirect ? redirect : data.signinUser.__typename == 'Store'? '/negocios/perfil' : '/'}`); 
        });
    }

    return (
        <Container maxWidth="sm">
            <h1>Ingresar</h1>
            <form onSubmit={validateAllInputs}>
                <Box mb={4}>
                    <Tooltip 
                        title={errors.message || ""} 
                        arrow
                        open={errors.path === 'email' && !!errors.errors[0]}
                        placement="top"
                    >
                        <TextField 
                            id="input-with-icon-grid"
                            type="tel"
                            style={{ width: 250 }}
                            variant="outlined"
                            label="Email" 
                            placeholder="Tu Correo Eletrónico"
                            name="email"
                            onChange={handleChange}
                            InputProps={{
                                startAdornment: (
                                <InputAdornment position="start">
                                    <MailOutlineIcon />
                                </InputAdornment>
                                ),
                            }}
                            error={errors.path === 'email' && !!errors.errors[0]}
                        />
                    </Tooltip>
                </Box>

                <Box mb={6}>
                    <Tooltip 
                        title={errors.message || ""} 
                        arrow
                        open={errors.path === 'password' && !!errors.errors[0]}
                    >
                        <FormControl variant="outlined" style={{ width: 250 }}>
                            <InputLabel htmlFor="password">Tu Contraseña</InputLabel>
                            <OutlinedInput
                                id="password"
                                type={ showPassword ? 'text' : 'password'}
                                value={formValues.password}
                                onChange={handleChange}
                                name="password"
                                endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="házlo visible"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                                }
                                labelWidth={160}
                                error={errors.path === 'password' && !!errors.errors[0]}
                            />
                        </FormControl>
                    </Tooltip>
                </Box>

                <Box mt={4}>
                    <Button
                        endIcon={<TelegramIcon />}
                        variant="contained"
                        color="primary"
                        size="large"
                        disabled={loading}
                        type="submit"
                        disabled={loading || validateForm()}
                    >
                        Ingresar 
                    </Button>
                </Box>
            </form>

            <Box mt={4}>
                <Link href="olvide-contrasena">
                    <a>
                        <small>Olvidé mi Contraseña</small>
                    </a>
                </Link>
            </Box>
        </Container>
        // <SigninWrap>
        //     <h1>Su gafete por favor</h1>

        //     <Form onSubmit={handleSubmit}>
        //         <FieldWrap>
        //             <input type="email" name="email" placeholder="Email" onChange={handleChange} value={formValues.email} />
        //         </FieldWrap>

        //         <FieldWrap>
        //             <PasswordEye 
        //                 name="password" 
        //                 placeholder="Contraseña" 
        //                 onChange={handleChange} 
        //                 value={formValues.password} 
        //             />
        //         </FieldWrap>
                
        //         <FieldWrap>
        //             <button 
        //                 className="ui button" 
        //                 type="submit"
        //                 disabled={loading || validateForm()}
        //             >Enviar</button>
        //             { error && <Error error={error}/> }
        //         </FieldWrap>
        //     </Form>

        //     <Link href="olvide-contrasena">
        //         <a className="underlined">
        //             <small>Olvidé mi Contraseña</small>
        //             </a>
        //     </Link>
        // </SigninWrap>
    )
}



export default withApollo({ ssr: true })(Ingresar);

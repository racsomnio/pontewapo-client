import { useState, useEffect } from 'react'
import Router from 'next/router'
import { withApollo } from "../../utils/apollo";
import { useMutation } from "@apollo/react-hooks"
import {CREATE_STORE, GET_CURRENT_USER} from '../../queries';
import styled from 'styled-components'
// import Error from '../../components/common/Error'
// import Tooltip from '../../components/common/Tooltip'
import { string, object } from 'yup';
// import dynamic from 'next/dynamic'
// const PlaceAutocomplete = dynamic(
//     () => import('../../components/common/PlaceAutocomplete'),
//     { ssr: false }
// ) //Made it dynamic so it doesnt break due to waiting for js


import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box'
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import Button from '@material-ui/core/Button'
import CreateIcon from '@material-ui/icons/Create';
import Autocomplete from '@material-ui/lab/Autocomplete';
import InputAdornment from '@material-ui/core/InputAdornment'
import { countryToFlag, countries } from '../../utils';
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton'
import FormControl from '@material-ui/core/FormControl'
import SnackBar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert';
import Tooltip from '@material-ui/core/Tooltip'

// const initialState = {
//     name: '',
//     typeOfBusiness: [],
//     phone: [{number: ''}],
//     location: {
//         address: '',
//         coordinates: [0,0]
//     },
//     email: '',
//     password: ''
// }

const initialState = {
    country: {
        code: '',
        label: '',
        phone: ''
    },
    email: '',
    password: ''
}

const validationSchema = object().shape({
    country: object().shape({ label: string().required('Selecciona el país donde se encuentra tu negocio')}),
    email: string().email('Este correo no es válido').required('Tu correo es necesario'),
    password: string().min(6, 'Mínimo 6 letras, números ó caracteres.'),
    // typeOfBusiness: string().required('Selecciona tu Tipo de Establecimiento'),
    // location: object().shape({ address: string().required('Tu Dirección es necesaria') }),
    // name: string().required('Un nombre es necesario'),
});

const Registro = () => {
    const [formValues, setFormValues] = useState(initialState);
    const [ errors, setErrors ] = useState({});
    const [ showPassword, setShowPassword ] = useState(false)
    const [ openSnackBar, setOpenSnackBar] = useState(false)

    function handleChange(e) {
        e.persist();
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            [name]: value
        }));
    }

    function handleAutocomplete(value) {
        if(value === null) value = initialState;
        const {type, ...rest} = value;
        setFormValues({ ...formValues, country: rest })
    }

    async function validateInputs (e) {
        const { name, value } = e.target;
        await validationSchema.validateAt(name, formValues).then(valid => {
            // console.log('valid')
            }).catch(err => {
            console.log('err:', err)
        });
    }

    async function validateAllInputs (e) {
        e.preventDefault();
        await validationSchema.validate(
            formValues,
            // { abortEarly: false }
        ).then(valid => {
            console.log('valid');
            setErrors({})
            // handleSubmit(e);
          }).catch(err => {
            console.log('err:', err)
            setErrors(err)
        })
    }
    
    function handleSubmit(e) {
        createStore().then(async({data}) => {
            clearState();
            Router.push(`${data.createStore.__typename == 'Store'? '/negocios/perfil' : '/'}`); 
        });
    }

    // function handleClickBusiness(e) {
    //     e.persist();
    //     const { value } = e.target.previousSibling;
    //     const isThere = formValues.typeOfBusiness.includes(value);

    //     isThere 
    //         ? setFormValues(prevState => ({
    //             ...prevState,
    //             typeOfBusiness: prevState.typeOfBusiness.filter( b => b !== value)
    //         }))
    //         : setFormValues(prevState => ({
    //             ...prevState,
    //             typeOfBusiness: [...prevState.typeOfBusiness, value]
    //         }))
    // }

    // function handlePhone(e) {
    //     const {name, value} = e.target;
    //     setFormValues(prevState =>({
    //         ...prevState,
    //         phone: [{number: value}]
    //     }));
    // }

    // function addLocation(location) {
    //     setFormValues(prevState =>({
    //         ...prevState,
    //         location
    //     }));
    // }

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    function clearState() {
        setFormValues({...initialState});
    }

    const handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpenSnackBar(false);
    };

    const [createStore, {error, loading}] = useMutation(CREATE_STORE, {
        variables: formValues,
        refetchQueries: [{query: GET_CURRENT_USER}],
        // awaitRefetchQueries: true,
    });

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
      }

    // const businesses = [
    //     "restaurante",
    //     "bistro",
    //     "fonda",
    //     "food-truck",
    //     "local",
    //     "puesto",
    //     "taquería",
    //     "comida-rápida",
    //     "pizzería",
    //     "hamburguesería",
    //     "marisquería",
    //     "panadería",
    //     "repostería",
    //     "cafetería",
    //     "casa-de-té",
    //     "heladería",
    //     "bar",
    //     "antro",
    //     "casino",
    //     "billar",
    //     "otro",
    // ]

    // const BusinessList = businesses.map( business => {
    //     const businessName = business.replace(/-/g, " ");
    //     return <div key={business}>
    //             <input type="checkbox" id={business} value={business} name="typeOfBusiness"></input>
    //             <label htmlFor={business} onClick={handleClickBusiness}> {businessName}</label>
    //         </div>
    // });

    return (
        <Box>
            <h1>Regístra tu Negocio</h1>

            <Form 
                onSubmit={validateAllInputs}
            >
                <Fields>
                    <Box mb={4} px={2}>
                        <Tooltip 
                            title={errors.message || ""} 
                            arrow
                            open={errors.path === 'country.label' && !!errors.errors[0]}
                        >
                            <Autocomplete
                                id="country"
                                style={{ width: 250 }}
                                options={countries}
                                autoHighlight
                                getOptionLabel={(option) => option.label}
                                renderOption={(option) => (
                                    <React.Fragment>
                                        <span>{countryToFlag(option.code)}</span>
                                        {option.label}
                                    </React.Fragment>
                                )}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="País"
                                        variant="outlined"
                                        inputProps={{
                                            ...params.inputProps,
                                            autoComplete: 'new-password', // disable autocomplete and autofill
                                        }}
                                        error={errors.path === 'country.label' && !!errors.errors[0]}
                                    />
                                )}
                                onChange={(event, newValue) => handleAutocomplete(newValue)}
                            />
                        </Tooltip>
                    </Box>
                    
                    <Box mb={4}>
                        <Tooltip 
                            title={errors.message || ""} 
                            arrow
                            open={errors.path === 'email' && !!errors.errors[0]}
                            placement="top"
                        >
                            <TextField 
                                id="input-with-icon-grid"
                                type="tel"
                                style={{ width: 250 }}
                                variant="outlined"
                                label="Email" 
                                placeholder="Tu Correo Eletrónico"
                                helperText="Este correo no será visible a tus clientes"
                                name="email"
                                onChange={handleChange}
                                InputProps={{
                                    startAdornment: (
                                    <InputAdornment position="start">
                                        <MailOutlineIcon />
                                    </InputAdornment>
                                    ),
                                }}
                                error={errors.path === 'email' && !!errors.errors[0]}
                            />
                        </Tooltip>
                    </Box>
                    

                    <Box mb={4}>
                        <Tooltip 
                            title={errors.message || ""} 
                            arrow
                            open={errors.path === 'password' && !!errors.errors[0]}
                        >
                            <FormControl variant="outlined" style={{ width: 250 }}>
                                <InputLabel htmlFor="password">Crea Una Contraseña</InputLabel>
                                <OutlinedInput
                                    id="password"
                                    type={ showPassword ? 'text' : 'password'}
                                    value={formValues.password}
                                    onChange={handleChange}
                                    name="password"
                                    endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="házlo visible"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                    }
                                    labelWidth={160}
                                    error={errors.path === 'password' && !!errors.errors[0]}
                                />
                            </FormControl>
                        </Tooltip>
                    </Box>

                    <Box mt={4}>
                        <Button
                            endIcon={<CreateIcon />}
                            variant="contained"
                            color="primary"
                            size="large"
                            disabled={loading}
                            type="submit"
                        >
                            Regístrame
                        </Button>
                    </Box>
                </Fields>
                {/* <SnackBar open={!!errors.errors} autoHideDuration={5000} onClose={handleCloseSnackBar}>
                    <Alert onClose={handleCloseSnackBar} severity="error">
                        {errors.message}
                    </Alert>
                </SnackBar> */}
                

                {/* <Fields>
                    <div className="field">
                        <Label>Número de WhatsApp</Label>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message={[<strong key="tt-phone">Verifica</strong>,". Teléfono para información y/o pedidos."]}
                            error={errors.path === 'phone[0].number' && errors.errors[0]}
                        >
                            <input type="tel" name="phone" placeholder="Teléfono del Negocio" onChange={handlePhone} onBlur={validateInputs} value={formValues.phone[0].number} />
                        </Tooltip>
                    </div>
                    <div className="field">
                        <Label>Crea una Contraseña</Label>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message={[<strong key="tt-password">Verifica</strong>,". Mínimo 6 letras, números ó caracteres."]}
                            error={errors.path === 'password' && errors.errors[0]}
                        >
                            <PasswordEye 
                                name="password" 
                                placeholder="Contraseña" 
                                onChange={handleChange} 
                                value={formValues.password} 
                                onBlur={validateInputs}
                            />
                        </Tooltip>
                    </div>

                    <div className="field">
                        
                        <Label>Nombre de tu negocio</Label>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message={[<strong key="tt-name">Verifica</strong>,". Este nombre no se puede cambiar en el futuro."]}
                            error={errors.path === 'name' && errors.errors[0]}
                        >
                            <input type="text" name="name" placeholder="Ej: Los Azules" onChange={handleChange} onBlur={validateInputs} value={formValues.name} />
                        </Tooltip>
                    </div>
                    <div className="field full">
                        <Label>Escribe y selecciona la dirección de tu negocio</Label>
                        <Tooltip 
                            showOnHover={false}
                            error={errors.path === 'location.address' && errors.errors[0]}
                        >
                            <PlaceAutocomplete addLocationToParentState={addLocation} />
                        </Tooltip>
                    </div>
                    <div className="field full">
                        <Label>Selecciona categoría(s)</Label>
                        <CheckboxList>
                            {BusinessList}
                        </CheckboxList>
                    </div> 
                </Fields> */}

                {/* <button 
                    className="ui button" 
                    type="submit"
                    disabled={loading}
                >
                    Enviar
                </button>
                { error && <Error error={error}/> } */}
            </Form>
        </Box>
    )
}

const Form = styled.form`
    max-width: 700px;
    margin: auto;
`;

// const CheckboxList = styled.div`
//     font-size: 0.8rem;
//     display: flex;
//     flex-wrap: wrap;
//     margin-bottom: 1rem;
//     padding-bottom: 2rem;
//     border-bottom: 1px solid #e6e7e8;

//     input[type="checkbox"] {
//         display:none;
//         width: 0;
//         height: 0;

//         & + label {
//            background: #fff;
//            padding: 0.5rem;
//            border-radius: 25px;
//            border: 1px solid #e6e7e8;
//            display: block;
//            margin: 0.4rem;
//            text-transform: capitalize;
//            font-weight: bold;
//            color: #999;
//            cursor: pointer;
//         }

//         &:checked + label {
//             background: #7a1a7b;
//             color: #fff;
//         }
//     }
// `;

// const Label = styled.label`
//     display: block;
//     font-size: 0.8rem;
//     margin-bottom:0.2rem;
//     font-weight: bold;
// `;

const Fields = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
`;

export default withApollo({ ssr: true })(Registro);

import { withApollo } from '../../utils/apollo'
import OnliIfLogin from '../../components/common/OnlyIfLogin'
import ShowProfile from '../../components/store/ShowProfile'

function Perfil() {
    return (
        <OnliIfLogin>
            <ShowProfile/>
        </OnliIfLogin>
    )
}

export default withApollo()(Perfil);

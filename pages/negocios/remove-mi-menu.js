import styled from 'styled-components'
import { useQuery } from '@apollo/react-hooks'
import OnlyIfLogin from '../../components/common/OnlyIfLogin'
import { withApollo } from '../../utils/apollo'
import MyMenuCreator from '../../components/menu/MyMenuCreator'
import { GET_CURRENT_USER} from '../../queries'
import Loading from '../../components/common/Loading'
import Error from '../../components/common/Error'

function AddMenu() {
    const { data, loading, error } = useQuery(GET_CURRENT_USER)

    if(loading) return <Loading />
    if(error) return <Error error={error} />

    return (
        <OnlyIfLogin>
            <h1>Menú Digital</h1>
            <Grid>
                {
                    data.getCurrentUser &&
                    <MyMenuCreator slug={ data.getCurrentUser.slug } />
                }
            </Grid>
        </OnlyIfLogin>
    )
}

export default withApollo({ ssr: true })(AddMenu)

const Grid = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
`;

import { useEffect } from 'react'
import PropTypes from 'prop-types';
import Router, { useRouter } from 'next/router'
import Head from 'next/head'
import { useTransition, animated } from 'react-spring'
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Layout from '../components/layout'
import * as gtag from '../utils/gtag'
import LoadingPage from '../components/common/LoadingPage'
import theme from '../styles/theme';

Router.onRouteChangeStart = () => {
    document.body.classList.add('loading-page');
};
Router.onRouteChangeComplete = () => {
    document.body.classList.remove('loading-page');
};
Router.onRouteChangeError = () => {
    document.body.classList.remove('loading-page');
};

export default function App({ Component, pageProps }) {
    const location  = useRouter()
    const transitions = useTransition( location, location => location.asPath, {
        from: { opacity: 0 },
        enter: { opacity: 1 },
        leave: { visibility: 'hidden'}
    })

    useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
          jssStyles.parentElement.removeChild(jssStyles);
        }

        // GA
        const handleRouteChange = (url) => {
            gtag.pageview(url)
        }
        Router.events.on('routeChangeComplete', handleRouteChange)
        
        // Clear it
        return () => {
          Router.events.off('routeChangeComplete', handleRouteChange)
        }
    }, [])

    return (
        <ThemeProvider theme={theme}>
            <Layout location={location.route}>
                <Head>
                    <title>Ponte WApo</title>
                    <link rel="icon" href="/nav-icons/ico.png" />
                    <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
                </Head>
                
                    {transitions.map(({ item, props, key }) => (
                        <animated.div key={key} style={props}>
                            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                            <CssBaseline />
                            <Component {...pageProps} />
                        </animated.div>
                    ))}
                <LoadingPage/>
            </Layout>
        </ThemeProvider>
    )
}

App.propTypes = {
    Component: PropTypes.elementType.isRequired,
    pageProps: PropTypes.object.isRequired,
};
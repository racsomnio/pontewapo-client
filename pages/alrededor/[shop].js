import { useRouter } from 'next/router'
import { withApollo } from "../../utils/apollo"
import FullInfo from '../../components/store/FullInfo'

function GetMenu() {
    const router = useRouter()
    const { shop } = router.query;
    return (
        <>
            <FullInfo slug={shop} />
        </>
    )
}

export default withApollo({ ssr: true })(GetMenu);
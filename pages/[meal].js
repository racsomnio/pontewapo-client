import { useRouter } from 'next/router'
import { withApollo } from "../utils/apollo"
import FullStoreInfo from '../components/digitalMenu/FullStoreInfo'


function GetDigitalMenu() {
    const router = useRouter()
    const { meal } = router.query;

    return (
        <>
            {
              meal &&
              <FullStoreInfo slug={meal} />
            }
        </>
    )
}

export default withApollo({ ssr: true })(GetDigitalMenu);
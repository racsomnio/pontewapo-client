import {useEffect, useState} from 'react'
import Link from 'next/link'
import Head from 'next/head'
import dynamic from 'next/dynamic'
import Button from '@material-ui/core/Button';
import JumboBox from '../components/home/JumboBox'

const Explore = dynamic(
  () => import('../components/home/Explore'),
  { loading: () => <p>...</p> }
)

const Pedidos = dynamic(
  () => import('../components/home/Pedidos'),
  { loading: () => <p>...</p> }
)

const AskGeolocation = dynamic(() => import('../components/common/AskGeolocation'), { ssr: false })

export default function Home() {
  const [coords, setCoords] = React.useState('');
  const [width, setWidth] = useState(false);

  function updateMedia() {
    setWidth(window.innerWidth >= 768);
  };

  useEffect(() => {
    updateMedia();
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  });

  function updateCoords(coords){
    setCoords(coords)
  }

  return (
    <>
      <Head>
        {/* <meta name="description" content="Satisface tus antojos dentro del rango de tu habitat laboral sin necesidad de pagar de más, sin tarjeta bancaria y todo desde tu teléfono. Ya sea comida, desayuno, cena, café, antojitos, postres o tragos." /> */}
        <meta name="description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta itemProp="name" content="Comida Godín" />
        <meta itemProp="description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta itemProp="image" content="https://comidagodin.com/home/comidagodin.jpg" />
        <meta property="og:url" content="https://comidagodin.com" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Comida Godín" />
        <meta property="og:description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta property="og:image:secure_url" content="https://comidagodin.com/home/comidagodin.jpg" />
        <meta property="og:image:width" content="800" />
        <meta property="og:image:height" content="467" />
        <meta property="og:locale" content="es_MX" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Comida Godín" />
        <meta name="twitter:description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta name="twitter:image" content="https://comidagodin.com/home/comidagodin.jpg" />
        <meta name="keywords" content="comida godin, menu digital, pedidos por whatsapp"/>
      </Head>

      <JumboBox />

      <Explore />

      <Pedidos />

      {/* <AskGeolocation coords={coords} updateCoords={updateCoords} /> */}

    </>
  )
}

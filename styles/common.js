import styled from 'styled-components';

export const Icon = styled.span`
    width: ${props => props.size || '30px'};
    display: ${({block}) => block ? 'block' : 'inline-block'};
    margin: auto;
   
    & + span{
        margin-left: 0.5rem;
    } 

    img, & + span {
        vertical-align: middle;
    }
    
    .isClickable{
        cursor: pointer
    }
`;

export const Ul = styled.ul`
    padding-left: 0;

    li {
        list-style: none;
    }
`;

export const ImgContainer = styled.div`
    position: relative;
    margin: auto auto 2rem;
    max-width: ${props => props.maxWidth || 'initial'};
`;

export const Divider = styled.div`
    height: 1px;
    background: radial-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0) );
    margin: 1.5rem 0;
`;

export const H1 = styled.h1`
    text-transform: capitalize;
    font-family: 'Baloo 2',cursive;
    margin: ${({margin}) => margin || '0 0 1rem'};

    span {
        display: block;
        font-size: 0.8rem;
    }
`;

export const GridCols = styled.div`
    display: flex;
    justify-content: ${props => props.align || 'space-evenly'};
    padding: ${ props => props.padding || 'initial' };
    margin: 1rem auto;
    white-space: normal;
    flex-wrap: wrap;
    max-width: 960px;

    .col {
        display: flex;
        flex-direction: column;
        justify-content: center;
        padding: ${ ({colPadding}) => colPadding || 'initial'};
    }
`;

export const ButtonLike = styled.a`
    background: rgb(242, 242, 242);
    color: #000;
    text-transform: capitalize;
    font-family: 'Baloo 2',cursive;
    padding: 0.5rem;
    border-radius: 3rem;
    cursor: pointer;
`;

export const Steps = styled.div`
    color: #999;
    font-size: 0.8rem;
    margin-top: 1rem;
`;


export const Checkbox = styled.label`
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
    margin-left: 0.5rem;
    float: ${ props => props.float || 'right'};

    & input { 
        opacity: 0;
        width: 0;
        height: 0;
        display: none;

        &:checked + .slider {
            background-color: #7a1a7b;
            user-select: none;
            -webkit-tap-highlight-color: transparent;

            &:before {
                transform: translateX(26px);
            }
        }

        &:focus + .slider {
            box-shadow: 0 0 1px #7a1a7b;
        }

    }

    .slider {
        position: relative;
        cursor: pointer;
        background-color: #ccc;
        border-radius: 34px;
        transition: .4s;
        width: inherit;
        height: inherit;
        display: inherit;
        vertical-align: middle;

        &:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            border-radius: 50%;
            transition: .4s;
        }
    }
`;
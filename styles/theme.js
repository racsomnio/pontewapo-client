import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#309668',
    },
    secondary: {
      main: '#fff',
    },
    terciary: {
      main: '#ACBDEB',  
    },
    alert: {
      main: '#ED4D4D',
    },
    background: {
      default: '#fff',
    },
    background_2nd: {
        main: '#F5F5F5'
    },
    text: {
        default: '#000',
    },
    borders: {
        default: '#777',
    }
  },
  shape: {
    borderRadius: 20,
  }, 
  typography: {
    button: {
      textTransform: 'none'
    }
  },
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: "0.8em",
        padding: '0.6rem 1rem',
      },
    }
  }
});

export default theme;